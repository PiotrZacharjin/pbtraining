package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DodawanieTest {


    @Test
    void przetestuj_dodawanie(){
        //given
        Dodawanie tested = new Dodawanie(5d,10d);
        //when
        Double wynik = tested.przeprowadzOperacje();
        //then
        assertEquals(15, wynik);
    }

    @Test
    void przetestuj_dodawanie_jesliLiczby_bedaNaPrzemian(){
        //given
        Dodawanie tested = new Dodawanie(10d,5d);
        //when
        Double wynik = tested.przeprowadzOperacje();
        //then
        assertEquals(15, wynik);
    }
}