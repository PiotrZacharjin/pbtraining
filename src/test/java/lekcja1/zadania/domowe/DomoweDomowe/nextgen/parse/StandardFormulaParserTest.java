package lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class StandardFormulaParserTest {
    InputParser testWpisywania1 = new StandardFormulaParser();
    StandardFormulaParser testtestu2 = new StandardFormulaParser();

    // FAIL testy
    @Test
    void testTesttestu() {
        //given
        String teksty = "a+2";

        //when
        boolean wynik2 = testtestu2.numberChecker(teksty);

        //then
        assertEquals(false, wynik2);
    }
//testy błędów na potem
//    @Test
//    void testujemyBladWpisania_P2() {
//        //given
//
//        String tekst = "+2";
//        //when
//        OperationComponentsDTO wynik = testWpisywania1.parse(tekst);
//        //then
//        assertEquals(null, wynik.getA());
//    }
//
//    //
//    @Test
//    void testujemyBladWpisania_123456789() {
//        //given
//
//        String tekst = "123456789";
//        //when
//        OperationComponentsDTO wynik = testWpisywania1.parse(tekst);
//        //then
//        assertEquals(null, wynik.getA());
//    }


    @Test
    void testujemyWpisywanie5p10a() {
        //given

        String tekst = "5+10";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(5, wynik.getA());
    }

    @Test
    void testujemyWpisywanie5p10b() {
        //given

        String tekst = "5+10";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(10, wynik.getB());
    }

    @Test
    void testujemyWpisywanie5p10o() {
        //given

        String tekst = "5+10";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals("+", wynik.getOperator().getOperator());
    }

    @Test
    void testujemyDLugiciag1123p312o() {
        //given

        String tekst = "123+312";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals("+", wynik.getOperator().getOperator());
    }

    @Test
    void testujemyDLugiciag123p312a() {
        //given

        String tekst = "123+312";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(123, wynik.getA());
    }


    @Test
    void testujemyDLugiciag123p312b() {
        //given

        String tekst = "123+312";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(312, wynik.getB());
    }

    @Test
    void testDziesietnych() {
        //given

        String tekst = "125.5*12.3";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(125.5, wynik.getA());
    }

    //dziesiętne mnożenie
    @Test
    void testDziesietnychmnozenieB() {
        //given

        String tekst = "125.5*12.3";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(12.3, wynik.getB());
    }

    @Test
    void testDziesietnychmnozenieA() {
        //given

        String tekst = "125.5*12.3";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(125.5, wynik.getA());
    }

    @Test
    void testDziesietnychOperator() {
        //given

        String tekst = "125.5*12.3";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals("*", wynik.getOperator());
    }
//testy dziesiętnych Dzielenie

    @Test
    void testDziesietnychdzielenieO() {
        //given

        String tekst = "125.5/12.3";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals("/", wynik.getOperator());
    }

    @Test
    void testDziesietnychdzielenieA() {
        //given

        String tekst = "125.5/12.3";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(125.5, wynik.getA());
    }

    @Test
    void testDziesietnychdzielenieB() {
        //given

        String tekst = "125.5/12.3";
        //when
        FormulaDTO wynik = testWpisywania1.parse(tekst);
        //then
        assertEquals(12.3, wynik.getB());
    }
}