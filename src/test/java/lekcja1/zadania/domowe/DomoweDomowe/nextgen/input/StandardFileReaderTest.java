package lekcja1.zadania.domowe.DomoweDomowe.nextgen.input;

import org.junit.jupiter.api.Test;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

class StandardFileReaderTest {


    TerminalInputReader testWpisywania1 = create();

    private TerminalInputReader create() throws FileNotFoundException {
        FileInputStream fileInputStream = new FileInputStream("src/main/plik.txt");
        return new StandardFileReader(fileInputStream);
    }

    StandardFileReaderTest() throws FileNotFoundException {
    }


    @Test
    void TestZczytaniaPliku1p2() throws IOException {
        //given
        String tekst = "1+2";
        //when
        String tekst2 = testWpisywania1.read();
        //then
        assertEquals(tekst, tekst2);

    }

    @Test
    void TestZczytaniaPliku1m2() throws IOException {
        //given
        String tekst = "1-2";
        //when
        String tekst2 = testWpisywania1.read();
        //then
        assertFalse(tekst.equals(tekst2));

    }

    @Test
    void TestZczytaniaPliku2() throws IOException {
        //given
        String tekst = "2+1";
        //when
        String tekst2 = testWpisywania1.read();
        //then
        assertEquals(tekst, tekst2);

    }


}