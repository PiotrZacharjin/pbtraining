package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class DzielenieTest {

    @Test
    void testDzieleniaNormalny() {
        //given
        Dzielenie test = new Dzielenie(10d, 5d);
        //when
        Double wynikDzielenia1 = test.przeprowadzOperacje();
        //then
        assertEquals(2, wynikDzielenia1);

    }

    @Test
    void testDzieleniaPrzemiennie() {
        //given
        Dzielenie test2 = new Dzielenie(5d, 10d);
        //when
        Double wynikDzielenia2 = test2.przeprowadzOperacje();
        //then
        assertEquals(0.5d, wynikDzielenia2);
    }

    @Test
    void testDzieleniaPrzezZero() {
        //given
        Dzielenie test3 = new Dzielenie(0d, 10d);
        //when
        Double wynik3 = test3.przeprowadzOperacje();
        //then
        assertEquals(0, wynik3);

    }

    @Test
    void testDzieleniaPrzezZero2() {
        //given
        Dzielenie test4 = new Dzielenie(10d, 0d);
        //when
        Double wynik4 = test4.przeprowadzOperacje();
        //then
        assertEquals(null, wynik4);

    }
}