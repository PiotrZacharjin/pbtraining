package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class OdejmowanieTest {

    @Test
    void przetestujOdejmowanie() {
        //given |Co ma testować
        Odejmowanie test = new Odejmowanie(10d, 5d);
        //when|zdażenie
        Double wynik = test.przeprowadzOperacje();
        //then |co sie wydazy
        assertEquals(5, wynik);
    }

    @Test
    void przetestujOdwrotneODejmowanie() {
        //given
        Odejmowanie test = new Odejmowanie(5d, 10d);
        //when
        Double odwrotnyWynik = test.przeprowadzOperacje();
        //then
        assertEquals(-5, odwrotnyWynik);
    }

    @Test
    void przetestujZeroMinusZero() {
        //given
        Odejmowanie testZero = new Odejmowanie(0d, 0d);
        //when
        Double zeroWynik = testZero.przeprowadzOperacje();
        //then
        assertEquals(0d, zeroWynik);
    }


}



