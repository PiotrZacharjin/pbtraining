package lekcja1.zadania.domowe.DomoweDomowe.nextgen.result;

import lekcja1.zadania.domowe.DomoweDomowe.nextgen.input.StandardKeyboardReader;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.input.TerminalInputReader;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja.Dodawanie;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja.Operacja;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.FormulaDTO;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.InputParser;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.StandardFormulaParser;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.operator.FormulaOperator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PrettyPrintTest {
    TerminalInputReader wpisywanaWartosc = new StandardKeyboardReader();
    InputParser analizowanaWartosc = new StandardFormulaParser();
    Operacja wykonywaneDzialenie = new Dodawanie(5d, 5d);
    //gdyby test wykonywałcałe zadanie
    ResultWriter drukowanyWynik = new PrettyPrint();
    FormulaResult testDTO = new FormulaResult(new FormulaDTO(5d, new FormulaOperator("+"), 5d), 10d);

//Próba 4


// ZLE! Zrobic tak że przez DTO wbijam gotowe wartości, czyli nadac wartości przez DTO a, o, b.
    // Następnie zrobić operacje i souta printem

    ;

    @Test
    void testPrettyPrint1() {
        //given
        new PrettyPrint().write(testDTO);
        //when
        //     FormulaResult wynikxxx = new PrettyPrint().write();
        //   String wynik = new FormulaResult(testDTO).getResult();
//then
        assertEquals("10", testDTO.getResult());
    }


}