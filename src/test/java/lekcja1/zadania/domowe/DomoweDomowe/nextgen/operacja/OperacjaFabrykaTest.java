package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.operator.FormulaOperator;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class OperacjaFabrykaTest {

    Double a = 0D;
    Double b = 0D;

    FormulaOperator operacja;
    private Operacja wynik;

    @Test
    void przetestuj_zwracaOperacjeDodawania() {
        //given
        operacja("+");
        //when
        przetestujFabryke();
        //then
        wynikTo(Dodawanie.class);
    }

    @Test
    void przetestuj_zwracaOperacjeOdejmowanie() {
        //given
        operacja("-");
        //when
        przetestujFabryke();
        //then
        wynikTo(Odejmowanie.class);
    }

    @Test
    void przetestuj_zwracaOperacjeDzielenie() {
        //given
        operacja("/");
        //when
        przetestujFabryke();
        //then
        wynikTo(Dzielenie.class);
    }

    @Test
    void przetestuj_zwracaOperacjeMnozenie() {
        //given
        operacja("*");
        //when
        przetestujFabryke();
        //then
        wynikTo(Mnozenie.class);
    }

    @Test
    void przetestuj_wywalkeJakNull() {
        //given
        operacja(null);
        //then
        assertThrows(RuntimeException.class, this::przetestujFabryke);

    }


    private void wynikTo(Class klasa) {
        assertEquals(klasa, wynik.getClass());

    }

    private void przetestujFabryke() {
        OperacjaFabryka operacjaFabryka = new OperacjaFabryka(a, operacja, b);
        wynik = operacjaFabryka.stworz();
    }

    private void operacja(String s) {
        this.operacja = new FormulaOperator(s);
    }


}