package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class MnozenieTest {

    //1 stworzenie obiektu testów
    @Test
    void mnozenieTest() {
        //given
        Mnozenie test = new Mnozenie(5d, 10d);
        //when
        Double wynikMnozenia = test.przeprowadzOperacje();
        //then
        assertEquals(50, wynikMnozenia);


    }

}