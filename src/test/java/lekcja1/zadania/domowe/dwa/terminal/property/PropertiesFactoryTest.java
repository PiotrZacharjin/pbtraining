package lekcja1.zadania.domowe.dwa.terminal.property;

import org.junit.Test;

import java.util.Map;

import static org.junit.Assert.assertEquals;

public class PropertiesFactoryTest {

    @Test
    public void test(){
        String password = create("user add --password=2323").get("password").getValue();
        assertEquals("2323",password);
    }

    @Test
    public void test2(){
        String password = create("user add --password=2323 --username=23132").get("password").getValue();
        assertEquals("2323",password);

    }

    @Test
    public void test3(){
        String username = create("user add --password=2323 --username=23132").get("username").getValue();
        assertEquals("23132",username);
    }

    private Map<String, PropertyDto> create(String s) {
        return new PropertiesFactory().create(s);
    }

}