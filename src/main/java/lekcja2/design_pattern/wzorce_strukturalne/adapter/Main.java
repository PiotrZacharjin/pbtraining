package lekcja2.design_pattern.wzorce_strukturalne.adapter;

public class Main {

    public static void main(String... args){
        LegacyService legacyService = new LegacyService();
        legacyService.calculateIndex(new DistanceInKilometers(19));

        Adapter adapter = new Adapter(new DistanceKilometers(10));
        legacyService.calculateIndex(adapter);
    }
}
