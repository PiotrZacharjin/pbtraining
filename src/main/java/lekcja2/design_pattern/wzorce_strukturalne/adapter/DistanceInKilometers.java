package lekcja2.design_pattern.wzorce_strukturalne.adapter;


import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DistanceInKilometers implements Distance {

    private double miles;
}
