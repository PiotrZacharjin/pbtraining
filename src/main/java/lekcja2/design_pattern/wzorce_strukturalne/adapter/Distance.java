package lekcja2.design_pattern.wzorce_strukturalne.adapter;

public interface Distance {

    double getMiles();
}
