package lekcja2.design_pattern.wzorce_strukturalne.adapter;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class DistanceKilometers {

    private float kilometers;

    public double getMiles() {
        return kilometers/ 1.61d;
    }
}
