package lekcja2.design_pattern.wzorce_strukturalne.adapter;

public class LegacyService {

    public double calculateIndex(Distance distance){
        double miles = distance.getMiles();
        return miles*10;
    }
}
