package lekcja2.design_pattern.wzorce_strukturalne.adapter;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Adapter implements Distance {
    private DistanceKilometers adaptee;

    @Override
    public double getMiles() {
        return adaptee.getKilometers()/1.61f;
    }
}
