package lekcja2.design_pattern.wzorce_kreacyjne.singleton;

public class Main {

    public static void main(String... args){
        WorldGovernmentService government = WorldGovernmentService.getInstance();
        government.addBounty(new Pirate("Luffy"),1500000);
    }
}
