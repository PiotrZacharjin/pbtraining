package lekcja2.design_pattern.wzorce_kreacyjne.singleton;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Pirate {
    private String name;
}
