package lekcja2.design_pattern.wzorce_kreacyjne.factory_method;

public class OldLeaseSystem {

    void lease(User user,Car car){
        int leaseCost = car.getLeaseCost();
        user.decreaseCashBy(leaseCost);

        /*
        if(isTruck()){
        ...

        if(isSHip(){
        ...

         */
    }
}
