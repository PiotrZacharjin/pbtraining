package lekcja2.design_pattern.wzorce_kreacyjne.factory_method;

public class User {
    private int credit;


    public User(int credit) {
        this.credit = credit;
    }

    public void decreaseCashBy(int decrease){
        this.credit -= decrease;
    }
}
