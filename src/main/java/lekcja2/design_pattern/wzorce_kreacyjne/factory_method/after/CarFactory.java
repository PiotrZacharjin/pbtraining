package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

public class CarFactory implements LeasedVehicleFactory {
    private String name;

    public CarFactory(String name) {
        this.name = name;
    }

    @Override
    public LeasedVehicle create() {
        return new NewCar(new MailService(),name);
    }
}
