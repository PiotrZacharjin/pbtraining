package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

import lekcja2.design_pattern.wzorce_kreacyjne.factory_method.User;

public class LeasingAction {

    private LeasedVehicleFactory vehicleFactory;

    public LeasingAction(LeasedVehicleFactory factory) {
        this.vehicleFactory = factory;
    }

    public void leaseTo(User requester){
        LeasedVehicle vehicle = vehicleFactory.create();
        vehicle.leaseTo(requester);
    }
}
