package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

import lekcja2.design_pattern.wzorce_kreacyjne.factory_method.User;

public class Ship implements LeasedVehicle {

    private int power;

    public Ship(int power) {
        this.power = power;
    }

    @Override
    public void leaseTo(User user) {
        user.decreaseCashBy(2000/ power);
    }
}
