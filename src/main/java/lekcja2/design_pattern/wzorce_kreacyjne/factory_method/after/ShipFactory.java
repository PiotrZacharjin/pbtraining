package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

public class ShipFactory implements LeasedVehicleFactory {
    private int horsePower;

    public ShipFactory(int horsePower) {
        this.horsePower = horsePower;
    }

    @Override
    public LeasedVehicle create() {
        return new Ship(horsePower);
    }
}
