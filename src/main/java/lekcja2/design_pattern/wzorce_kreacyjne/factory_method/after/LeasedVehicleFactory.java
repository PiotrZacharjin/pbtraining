package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

public interface LeasedVehicleFactory {

    LeasedVehicle create();
}
