package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;


import lekcja2.design_pattern.wzorce_kreacyjne.factory_method.User;

public interface LeasedVehicle {
    void leaseTo(User user);
}
