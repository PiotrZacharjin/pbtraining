package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

public class TruckFactory implements LeasedVehicleFactory {
    private int weight;

    public TruckFactory(int weight) {
        this.weight = weight;
    }

    @Override
    public LeasedVehicle create() {
        return new Truck(10, weight);
    }
}
