package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

import lekcja2.design_pattern.wzorce_kreacyjne.factory_method.User;

public class NewCar implements LeasedVehicle {

        private MailService service;
    private String name;

    public NewCar(MailService service, String name) {
        this.service = service;
        this.name = name;
    }

    @Override
    public void leaseTo(User user) {
        service.send(name + " leased");
        user.decreaseCashBy(20);
    }
}
