package lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after;

import lekcja2.design_pattern.wzorce_kreacyjne.factory_method.User;

public class Truck implements LeasedVehicle {

    private int wheelsAmount;
    private int weight;

    public Truck(int wheelsAmount, int weight) {
        this.wheelsAmount = wheelsAmount;
        this.weight = weight;
    }

    @Override
    public void leaseTo(User user) {
        user.decreaseCashBy(1000/wheelsAmount * weight);
    }
}
