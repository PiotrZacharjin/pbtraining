package lekcja2.design_pattern.wzorce_kreacyjne.factory_method;

import lekcja2.design_pattern.wzorce_kreacyjne.factory_method.after.*;

public class Main {

    ///inaczej Factory Method
    public static void main(String... args){
        //Metoda wytwórcza jest kreacyjnym wzorcem projektowym, który udostępnia interfejs do
        //tworzenia obiektów w ramach klasy bazowej, ale pozwala podklasom zmieniać typ tworzonych obiektów.

        //Problem:
        // Wyobrazmy sobie ze mamy system leasingu aut samochodowych. System dziala sprawnie

        User user = new User(400);
        OldLeaseSystem oldLeaseSystem = new OldLeaseSystem();
        Car firstCar = new Car("Volvo", 5,100);
        oldLeaseSystem.lease(user,firstCar);

        Car secondCar = new Car("Maluch", 2,200);
        oldLeaseSystem.lease(user,secondCar);
        //oldSystem.add(new Truck(" // nope
        // ale po czasie biznes oświadcza że potrzebujecie rozszerzyć działalność o kolejne brańże
        // Teraz potrzebujesz kolejne samochody które rzadza sie innymi zasadami

        // Co robisz?
        LeasedVehicleFactory vehicleFactory = new CarFactory("Volvo");
        new LeasingAction(vehicleFactory).leaseTo(user);

        vehicleFactory = new TruckFactory(45000);
        new LeasingAction(vehicleFactory).leaseTo(user);

        vehicleFactory = new ShipFactory(300);
        new LeasingAction(vehicleFactory).leaseTo(user);

    }
}
