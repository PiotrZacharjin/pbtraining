package lekcja2.design_pattern.wzorce_kreacyjne.factory_method;

public class Car {
    private String name;
    private int doorAmount;
    private int leaseCost;

    public Car(String name, int doorAmount, int leaseCost) {
        this.name = name;
        this.doorAmount = doorAmount;
        this.leaseCost = leaseCost;
    }

    public int getLeaseCost() {
        return leaseCost;
    }
}
