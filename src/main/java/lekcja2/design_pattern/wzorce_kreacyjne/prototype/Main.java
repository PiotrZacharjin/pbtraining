package lekcja2.design_pattern.wzorce_kreacyjne.prototype;

public class Main {

    public static void main(String[] args){
        OrangeWorker piotr = new OrangeWorker("Piotr", 33);
        OrangeWorker clone = piotr.clone();
        piotr.increaseAge();
        System.out.println(piotr.getAge());
        System.out.println(clone.getAge());
    }
}
