package lekcja2.design_pattern.wzorce_kreacyjne.prototype;

public interface Prototype<T> {

    T clone();
}
