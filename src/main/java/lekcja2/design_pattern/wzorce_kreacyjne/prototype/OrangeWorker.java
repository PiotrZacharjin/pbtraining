package lekcja2.design_pattern.wzorce_kreacyjne.prototype;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class OrangeWorker implements Prototype<OrangeWorker> {
    private String name;
    @Getter
    private Integer age;

    void increaseAge(){
        age++;
    }

    @Override
    public OrangeWorker clone() {
        return new OrangeWorker(name, age);
    }
}
