package lekcja2.design_pattern.wzorce_kreacyjne.builder;

public class Main {
    public static void main(String... args){
        Agreement firstAgreement = new AgreementBuilder()
                .perMonth()
                .build();

        Agreement secondAgreement = new AgreementBuilder()
                .setPhone(new Phone("Samsung"))
                .build();


        Agreement thirdAgreement = new AgreementBuilder()
                .setPhone(new Phone("Iphone"))
                .setPayment(40000)
                .build();
    }
}
