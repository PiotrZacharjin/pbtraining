package lekcja2.design_pattern.wzorce_kreacyjne.builder;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Agreement {
    private boolean stable;
    private Phone phone;
    private double paid;

}
