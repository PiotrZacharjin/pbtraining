package lekcja2.design_pattern.wzorce_kreacyjne.builder;

public class AgreementBuilder {

    private boolean stable;
    private Phone phone = Phone.empty();
    private double amountPaid;

    AgreementBuilder perMonth(){
        this.stable= true;
        return this;
    }
    AgreementBuilder setPhone(Phone phone){
        this.phone= phone;
        return this;
    }

    AgreementBuilder setPayment(double payment){
        this.amountPaid = payment;
        return this;
    }

    Agreement build(){
        return new Agreement(stable, phone, amountPaid);
    }

}
