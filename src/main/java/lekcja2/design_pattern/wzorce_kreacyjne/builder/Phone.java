package lekcja2.design_pattern.wzorce_kreacyjne.builder;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Phone {

    private String phone;

    public static Phone empty() {
        return new Phone("unassigned");
    }
}
