package lekcja1.metody;

import lekcja1.ify.Czlowiek;

public class Main {


    static String zrobCos(){
        return "cos";
    }

    public static void main(String[] args) {
        //metody moga być globalne (Statyczne) lub w obiekcie
        //metoda posiada zwracany element oraz parametry znajdujące się w klamrach
        //jesli metoda nie zwraca metody to zwraca void (nicość)
        //jesli metody coś zwraca to ma return na końcu)

        //metody najczesciej znajduja sie w obiektach (do tego potem dojdziemy)
        System.out.println(zadanie());
    }


    //ZADANIE
    // Stworz metode statyczna która zwraca wynik mnożenia zmiennych a i b i przypisuje ja do c
    public static Czlowiek zadanie() {
        Czlowiek robot = new Czlowiek();
        float a = 40;
        float b = 50;
        Float c = 0f;

        if (c == 2000) {
            robot.escapeFromEarth();
        }

        return robot;
    }
}
