package lekcja1.booleany;


public class Main {


    public static void main(String[] args) {
        //booleany to odpowiednik prawdy i fałszu
        //służa do sprawdzania logiki
        boolean a = true;
        Boolean b = false;
        //używany w ifach i petlach
        if(a){
            //jesli a jest true
        }
        while(b){
            //jesli b jest true to zapetl sie
        }

        //booleany mozna laczyc
        boolean c = a && b;  //logiczne stwierdzenie AND oba musza być true żeby wyszło true    false i true zwróci FALSE
        boolean d = a || b;  //logiczne stwierdzenie OR jeden z nich musi byc prawdziwy byc

        //mozemy robić operacje logiczne na liczbach np > , <,  == != >= <=
        //lub wywoływać metody na innych obiektach np .equals()

        //na przykład
        double wynagrodzenie = 1600;
        String imie = "Tomek";

        boolean prawda =  wynagrodzenie < 2000 && imie.equals("Tomek");
        System.out.println(zadanie(3000, 3200));
        System.out.println(zadanie(200, 200000));
    }


    //ZADANIE
    // Pan Robot chciałby żebyś stworzył funkcje która przyjmuje stare i nowe wynagordzenie
    // dzieli różnice przez 10 i sprawdza czy podwyżka jest mniejsza niż 30
    public static boolean zadanie(float wynagrodzenie, float noweWynagrodzenie) {

        return false;
    }

}
