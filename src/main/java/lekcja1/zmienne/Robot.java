package lekcja1.zmienne;

public class Robot {

    private Number kupa;

    public void setKupa(Number kupa) {
        this.kupa = kupa;
    }

    @Override
    public String toString() {
        return "Robot dostał od Piotrka "+ kupa +" GB pamięci" ;
    }
}
