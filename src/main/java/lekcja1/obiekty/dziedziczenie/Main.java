package lekcja1.obiekty.dziedziczenie;


import lekcja1.obiekty.interfejsy.Robot;
import lekcja1.obiekty.interfejsy.RobotWife;

public class Main {

    public static void main(String[] args) {
        //Tak jak w przypadku interfejsu, dziedziczenie też jest pewnym sposobem nadania
        // miejsca obiektowi w jakimś wiekszym procesie, który wymaga od obiektów pewnych
        //założeń

        //Klasa abstrakcyjna pozwala nam to osiągnąc
        //działą ona jak interfejs ale zawiera w sobie już jakiś kod
        // (implementujący pewną funkcjonalność)
        //zamiast implementować interfejs trzeba EXTENDOWAĆ tą klase (dziedziczyć)

       // Robot robot = new Robot();
       // robot.marry(zadanie());
      //  System.out.println(robot);
    }


    //ZADANIE
    // Robot szuka mądrej żony
    // stworz klase która extenduje abstrakcyjna klase AbsractWife
    public static AbstractWife zadanie() {

        return null;
    }



}
