package lekcja1.obiekty.dziedziczenie;

import lekcja1.obiekty.interfejsy.RobotWife;
import lekcja1.obiekty.interfejsy.Room;
import lekcja1.obiekty.interfejsy.Sandwitch;

public abstract class AbstractWife implements RobotWife {

    protected abstract Sandwitch createSuperDuperSandwich();


    @Override
    public Sandwitch createSandwich() {
        return createSuperDuperSandwich();
    }

    @Override
    public void clean(Room room) {

    }
}
