package lekcja1.obiekty.interfejsy.czlowiek;

public class Czlowiek implements Stworzenie{

    private final Integer wiek;
    private final ImieFabryka imieFactory;


    Czlowiek(Integer wiek, ImieFabryka imieFactory) {
        this.wiek = wiek;
        this.imieFactory = imieFactory;
    }


    public void podskocz() {
        System.out.println(imieFactory.create() + " podskoczyl");
    }

    @Override
    public String toString() {
        return imieFactory.create();
    }

    @Override
    public void oddychaj() {
        System.out.println("oddycham bo jestem czlowiekiem. identyfikuje sie jako stworzenie");
    }

    @Override
    public void chlac() {

    }

    @Override
    public void wydala() {
        System.out.println("Jasiu zrobił ładną kupkę");
    }

    @Override
    public void starzenie() {

    }
}
