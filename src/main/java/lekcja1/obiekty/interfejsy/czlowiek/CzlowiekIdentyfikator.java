package lekcja1.obiekty.interfejsy.czlowiek;

public class CzlowiekIdentyfikator {


    public boolean zidentifiukuj( Czlowiek czlowiek){
        boolean jestCzlowiekiem = czlowiek.toString().equals("jan");
        return jestCzlowiekiem;
    }
}
