package lekcja1.obiekty.interfejsy.czlowiek;

public interface Stworzenie {

    void oddychaj();

    void chlac();

    void wydala();

    void starzenie();
}
