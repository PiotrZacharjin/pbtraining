package lekcja1.obiekty.interfejsy;

public class SpotkanieFactory {
    private String spotkanie;

    public SpotkanieFactory(String spotkanie) {
        this.spotkanie = spotkanie;
    }


    public Wydarzenia create() {
        if (spotkanie.contains("Seans")) {
            return new Przeszkody();

        }
        if (spotkanie.contains("Oliwiarnia")) {
            return new Randka();
        } else System.out.println();
        throw new RuntimeException(spotkanie + " nie istnieje. Zły wybór, koniec gry");
    }
}
