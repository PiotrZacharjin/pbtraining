package lekcja1.obiekty.interfejsy;

public class WydarzeniaFactory {


    private String wydarzenie;

    public WydarzeniaFactory(String wydarzenie) {
        this.wydarzenie = wydarzenie;
    }


    public Wydarzenia create() {
        if (wydarzenie.contains("Przyjaciel")) {
            return new Przeszkody();

        }
        if (wydarzenie.contains("Terminal")) {
            return new Randka();
        } else System.out.println();
        throw new RuntimeException(wydarzenie + " nie istnieje. Zły wybór, koniec gry");
    }
}