package lekcja1.obiekty.tworzenie;

public class Czlowiek {

    private Integer wiek;
    private String imie;

    public Czlowiek(Integer wiek, String imie) {
        this.wiek = wiek;
        this.imie = imie;
    }


    public void podskocz() {
        System.out.println("Hurraa mam "+ wiek+" lat.");
    }
}
