package lekcja1.obiekty.tworzenie;


public class Main {


    public Main(String s, Integer integer) {

    }

    public static void main(String[] args) {

        //obiekty to cegiełki każdego oprogramowania. To próba ułożenia logiki w spójna całość
        // w taki sposob
        //by każdy jej element miał obiekt odpowiedzialny za ta logike


        //istnieja wbudowane obiekty jak
        String text;
        //albo customowowe pisane przez nas

        //KLASA to tak naprawde template obiektu. schemat

        //obiekt to instancja klasy. coś co już nie jest schematem a żyje własnym życiem

        //tworzymy klase class Nazwa {}

        //obiekt tej KLASY tworzymy new Nazwa();

        //obiekty tworzy konstruktor w klasie

        // Nazwa(...)

        // klasa ma swój stan zwany parametrami. maja one one określone dostępy
        //public protected private
        // przyjmij zasade że stan obiektu jest ZAWSZE private


       // System.out.println(Main.Nazwa.zadanie());

        Czlowiek piotrek = new Czlowiek(10, "Piotr");

        piotrek.podskocz();

    }


    //ZADANIE
    // Robot potrzebuje obiektów
    // zwróc mu swój customowy obiekt z konstruktorem
    // który przyjmuje STRING i INTEGER na wejsciu,
    //    // przypisuje je do wewnetrznych parametrow

    //+ daj mu publiczna metode podskocz()
//    static class Nazwa {
//        private String s;
//        private Integer integer;
//
//        public Nazwa(String s, Integer integer) {
//
//            this.s = s;
//            this.integer = integer;
//        }
//
//        @Override
//        public String toString() {
//            return "Nazwa{" +
//                    "s='" + s + '\'' +
//                    ", integer=" + integer +
//                    '}';
//        }
//
//        static Object zadanie() {
//            //Integer a =10;
//           // String b = "Turbo";
//            Nazwa fuel = new Nazwa("Turbo", 10);
//           //   System.out.println(fuel);
//            return fuel;
//        }
//    }

}
