package lekcja1.kolekcje;

import java.util.Objects;

public class RobotId {
    private final Integer id;
    private final String imie;

    public RobotId(Integer id, String imie) {
        this.id = id;
        this.imie = imie;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RobotId robotId = (RobotId) o;
        return Objects.equals(id, robotId.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, imie);
    }

    @Override
    public String toString() {
        return "RobotId{" +
                "id=" + id +
                ", imie='" + imie + '\'' +
                '}';
    }
}
