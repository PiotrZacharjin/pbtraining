package lekcja1.kolekcje;

public class PudzianRobot {
    private final RobotId id;


    public PudzianRobot(RobotId id) {
        this.id = id;
    }

    public RobotId getId() {
        return id;
    }

    @Override
    public String toString() {
        return "PudzianRobot{" +
                "id=" + id +
                '}';
    }
}
