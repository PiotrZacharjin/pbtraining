package lekcja1.kolekcje;

import java.util.*;

public class Main {


    static String zrobCos() {
        return "cos";
    }

    public static void main(String[] args) {
        //lista to kolekcja
        //do list możemy dodawać/ usuwać elementy
        //możemy również iterować po elementach listy

        List<Long> lista = new ArrayList<>();

        for(int i=0;i<lista.size();i++){
            Long element = lista.get(i);
            //iterowanie po liscie for
        }

        for (Long element : lista) {
            //iterowanie po liscie for prawilne
        }

        //w repertuarze kolekcji znajdują się również podobne klasy
        // ale różnia sie sposobem przechowywania obiektów, sposobem wyciagania ich z listy
        //np Set, gdy dodasz do Setu (zbioru) element który ma taki sam equals co inny wtedy nie zostanie dodany
        // np zbiór liczb
        HashSet<Long> set = new HashSet<>();
        set.add(23L);
        set.add(12L);
        //nie ma get - trzeba robic for each

        // mapa - HashMapa - przechowuje elementy po kluczu (niczym ID) i potrafi w błyskawicznym czasie
        //wyciagnać element po ID (iterowanie po
        HashMap<Integer, String> mapa = new HashMap<>();
        mapa.put(23, "Piotrek");
        mapa.put(30, "Tomek");
        mapa.put(123, "Basia");

        mapa.get(123); //To Basia


        //kolekcje możemy sortować przez podanie klasy komparatora (jaka ma być kolejność)
        lista.sort(new Comparator<Long>() {
            @Override
            public int compare(Long o1, Long o2) {
                return o1.compareTo(o2);
            }
        });

        System.out.println(zadanie6());
    }


    //ZADANIE
    // Pan Robot chciałby ułożyć swoje klony na liście
    // każdy klon ma swój identyfikator
    // lista powinna być posortowana po identyfikatorach
    public static List<Robot> zadanie() {
        Robot robot1 = new Robot(23);
        Robot robot2 = new Robot(24);
        Robot robot3 = new Robot(123);
        Robot robot4 = new Robot(23);
        //stworzenie listy klonów i dodanie żywego inwentarza
        ArrayList<Robot> magazynKlonow = new ArrayList<>();
        // List<Robot> magazynKlonow = Lists.newArrayList(
        magazynKlonow.add(robot1);
        magazynKlonow.add(robot2);
        magazynKlonow.add(robot3);
        magazynKlonow.add(robot4);
        //sortowanie
        // Collections.sort(magazynKlonow, Comparator.comparing(Robot::getId) );
        magazynKlonow.sort(Robot::compareTo);
        for (Robot i : magazynKlonow) {
//robot1.compareTo(robot2)

            if (i.getId() < 24) {
                continue;

            }
            System.out.println(i);
        }


//       // Collections.sort();
//        for(Integer s: ){
//
//            System.out.println(s);
//
//        }
        return null;
    }


    //ZADANIE
    // Pan Robot chciałby ułożyć swoje klony na Mapie
    // kluczem bedzie identyfikator

    public static Map<Integer, Robot> zadanie2() {
        Robot robot1 = new Robot(23);
        Robot robot2 = new Robot(24);
        Robot robot3 = new Robot(123);
        Robot robot4 = new Robot(23);
        //dodanie żywego inwentarza do magazynu NR2
        HashMap<Integer, Robot> magazynKlonow2 = new HashMap<>();
        magazynKlonow2.put(robot1.getId(), robot1);
        magazynKlonow2.put(robot2.getId(), robot2);
        magazynKlonow2.put(robot3.getId(), robot3);
        magazynKlonow2.put(robot4.getId(), robot4);
        //pobieranie
        // magazynKlonow2.get(23);
        Collection<Robot> values = magazynKlonow2.values();
        //System.out.println(magazynKlonow2.containsKey(55));
        // System.out.println(magazynKlonow2.get(666));
        System.out.println(values);
        return null;

    }

    public static Map<RobotId, PudzianRobot> zadanie5() {
        PudzianRobot robot1 = new PudzianRobot(new RobotId(1, "Pudzi1"));
        PudzianRobot robot2 = new PudzianRobot(new RobotId(2, "Pudzi2"));
        PudzianRobot robot3 = new PudzianRobot(new RobotId(3,"Pudzi3"));
        PudzianRobot robot4 = new PudzianRobot(new RobotId(4, "Pudzi4"));
        //dodanie żywego inwentarza do magazynu NR2
        HashMap<RobotId, PudzianRobot> farmaPudzianow = new HashMap<>();
        farmaPudzianow.put(robot1.getId(), robot1);
        farmaPudzianow.put(robot2.getId(), robot2);
        farmaPudzianow.put(robot3.getId(), robot3);
        farmaPudzianow.put(robot4.getId(), robot4);
        //pobieranie
        // magazynKlonow2.get(23);
        Collection<PudzianRobot> values = farmaPudzianow.values();
        //System.out.println(magazynKlonow2.containsKey(55));
        // System.out.println(magazynKlonow2.get(666));
        System.out.println(values);
        return null;

    }  public static Map<RobotId, PudzianRobot> zadanie6() {
        PudzianRobot robot1 = new PudzianRobot(new RobotId(1, "Pudzi1"));
        PudzianRobot robot2 = new PudzianRobot(new RobotId(1, "Pudzi2"));
        PudzianRobot robot3 = new PudzianRobot(new RobotId(3,"Pudzi3"));
        PudzianRobot robot4 = new PudzianRobot(new RobotId(4, "Pudzi4"));
        //dodanie żywego inwentarza do magazynu NR2
        HashSet<RobotId> farmaPudzianow = new HashSet<>();
        farmaPudzianow.add(robot1.getId());
        farmaPudzianow.add(robot2.getId());
        farmaPudzianow.add(robot3.getId());
        farmaPudzianow.add(robot4.getId());
        //pobieranie
        // magazynKlonow2.get(23);
        System.out.println(farmaPudzianow.size());

        return null;

    }

       //ZADANIE
    // Pan Robot chciałby ułożyć swoje klony na zbiór (Set)
    // unikalnościa bedzie ID, nie powinno być duplikatow
    public static Set<Robot> zadanie4() {
        Robot robot1 = new Robot(23);
        Robot robot2 = new Robot(23);
        Robot robot3 = new Robot(123);
        Robot robot4 = new Robot(23);

        return null;
    }
}
