package lekcja1.kolekcje;

import java.util.Objects;

public class Robot implements Comparable<Robot> {
    @Override
    public String toString() {
        return "Robot{" +
                "id=" + id +
                '}';
    }

    private Integer id;

    public Robot(Integer id) {
        this.id = id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Robot robot = (Robot) o;
        return Objects.equals(id, robot.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    public Integer getId() {
        return id;
    }

    public void escapeFromEarth() {
        throw new RuntimeException("Robot spierdolił z planety ziemi. Try again");
    }

    @Override
    public int compareTo(Robot zewnetrznyObiekt) {
        Integer porownanie = (Integer) id;
        Integer zewnetrzneId = (Integer) zewnetrznyObiekt.id;
        if (porownanie < zewnetrzneId) {
            return -1;
        }
        if (porownanie.equals(zewnetrzneId)) {
            return 0;

        }
        if (porownanie > zewnetrzneId){
            return 1;
        }
        return 0;
    }
}
