package lekcja1.ui;

import lekcja1.ui.chat.ChatWindow;
import lekcja1.ui.chat.ChatWindowFactory;
import lekcja1.ui.message.Message;
import lekcja1.ui.message.repo.MessageRepository;
import lekcja1.ui.message.repo.MessageRepositoryFactory;

import javax.swing.*;

public class UIApplication {
    public static void main(String[] args) {
        MessageRepository repo = new MessageRepositoryFactory().create();

        ChatWindow chatWindow = new ChatWindowFactory().create(repo);

        repo.add(Message.create("Robot", "Hello wold"));

        display(chatWindow);
    }

    private static void display(ChatWindow chatWindow) {
        JFrame frame=new JFrame("Gadu Gadu");
        frame.setContentPane(chatWindow.getContent());
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.pack();
        frame.setVisible(true);
    }



}
