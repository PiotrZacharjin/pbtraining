package lekcja1.ui.chat;

import lekcja1.ui.message.Message;
import lombok.AllArgsConstructor;

import javax.swing.*;
import java.util.List;

@AllArgsConstructor
public class ChatMessageContentFactory {

    private List<Message> messages;

    ListModel<Message> create(){
        DefaultListModel<Message> defaultListModel = new DefaultListModel<>();
        messages.forEach((defaultListModel::addElement));
        return defaultListModel;
    }
}
