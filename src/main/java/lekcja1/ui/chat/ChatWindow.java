package lekcja1.ui.chat;

import lekcja1.ui.message.Message;
import lombok.Getter;

import javax.swing.*;

@Getter
public class ChatWindow {
    private JTextField messageField;
    private JButton sendButton;
    private JList<Message> messages;
    private JPanel content;
}
