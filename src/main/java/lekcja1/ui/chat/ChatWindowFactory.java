package lekcja1.ui.chat;

import lekcja1.ui.message.Message;
import lekcja1.ui.message.repo.MessageRepository;
import lekcja1.ui.message.repo.OnChangeMessagesListener;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.util.List;

public class ChatWindowFactory {

    public ChatWindow create(MessageRepository repo){
        ChatWindow chatWindow = new ChatWindow();

        populateChatWithExistingMessages(chatWindow, repo.findAll());

        chatWindow.getSendButton()
                .addActionListener(createOnSendButtonClickListener(chatWindow, repo));

        repo.addListener(createChatWindowRefreshListener(chatWindow));

        return chatWindow;
    }

    private void populateChatWithExistingMessages(ChatWindow chatWindow, List<Message> messagesList) {
        ListModel<Message> messages = new ChatMessageContentFactory(messagesList).create();
        chatWindow.getMessages().setModel(messages);
    }

    private ActionListener createOnSendButtonClickListener(ChatWindow chatWindow, MessageRepository repo) {
        return event -> {
            String text = chatWindow.getMessageField().getText();
            repo.add(Message.create("You",text));
            chatWindow.getMessageField().setText("");
        };
    }


    private OnChangeMessagesListener createChatWindowRefreshListener(ChatWindow chatWindow) {
        return messages -> {
            ListModel<Message> messagesModel = new ChatMessageContentFactory(messages).create();
            chatWindow.getMessages().setModel(messagesModel);
        };
    }

}
