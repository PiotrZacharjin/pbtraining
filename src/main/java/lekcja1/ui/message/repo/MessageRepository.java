package lekcja1.ui.message.repo;

import lekcja1.ui.message.Message;
import lombok.AllArgsConstructor;

import java.util.List;


@AllArgsConstructor
public class MessageRepository {

    private List<Message> messages;
    private List<OnChangeMessagesListener> listeners;

    public List<Message> findAll(){
        return messages;
    }

    public void add(Message message){
        messages.add(message);
        listeners.forEach(listener->listener.onChange(messages));
    }

    public void addListener(OnChangeMessagesListener listener){
        this.listeners.add(listener);
    }
}
