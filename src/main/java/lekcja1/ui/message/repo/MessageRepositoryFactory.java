package lekcja1.ui.message.repo;

import java.util.ArrayList;

public class MessageRepositoryFactory {

    public MessageRepository create(){
        MessageRepository repo = new MessageRepository(new ArrayList<>(), new ArrayList<>());

        repo.addListener(createPrintEverythingListener());
        return repo;
    }


    private static OnChangeMessagesListener createPrintEverythingListener() {
        return messages -> {
            messages.forEach(System.out::println);
        };
    }
}
