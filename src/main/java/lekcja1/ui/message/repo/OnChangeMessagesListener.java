package lekcja1.ui.message.repo;

import lekcja1.ui.message.Message;

import java.util.List;

public interface OnChangeMessagesListener {

    void onChange(List<Message> messages);
}
