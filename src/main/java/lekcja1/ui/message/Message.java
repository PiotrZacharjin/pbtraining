package lekcja1.ui.message;

import lombok.AllArgsConstructor;
import lombok.Value;

import java.util.Date;

@Value
@AllArgsConstructor
public class Message {

    public static Message create(String author, String text){
        return new Message(author, new Date().toString(), text );
    }

    private String author;
    private String date;
    private String message;

    @Override
    public String toString() {
        return date+" "+author + ": " + message;
    }
}
