package lekcja1.zadania.zadanieDwa;

import java.util.Scanner;

public class RobotWorkMenager {
    Scanner input = new Scanner(System.in);

    // na podstawie true albo false kieruje do danej fabryki robota
    public void directingWork(){
        System.out.println("Witaj w programie pozwalającym na stworzenie odbicie wpisanego tesktu");
        System.out.println("Jeśli chcesz Odwrócić swój tekst wpisz \"tak\"");
        System.out.println("Jeśli nie, zostaw puste pole, robot przekaże Ci miłą sentecje");
        String text = input.nextLine();
        if (text.equalsIgnoreCase("")){
            new SentenceRobotFactory().create(false);
        }
        else new ConversionRobotFactory().create(true);

        }
    }

