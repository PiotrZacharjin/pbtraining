package lekcja1.zadania.zadanieDwa;

import java.util.Scanner;

public class ConversionRobotFactory implements RobotSlave{
    Scanner input = new Scanner(System.in);




    public void create(Boolean advanced){
        System.out.println("Podaj tekst do odwrócenia");
        String text = input.nextLine();
        text = revert(text);
        System.out.println(text);

    }


    private String revert(String text){
        StringBuilder reverse = new StringBuilder();
        reverse.append(text);
        reverse.reverse();
        return String.valueOf(reverse);
    }
}
