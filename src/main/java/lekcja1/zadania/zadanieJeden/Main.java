package lekcja1.zadania.zadanieJeden;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Main {

    public static void main(String[] args) {
        List<User> test = new ArrayList<>();
        test.add(new User(new Id(4L),"zenek"));
        test.add(new User(new Id(10L),"jakub"));
        test.add(new User(new Id(40L),"Prosiak"));
        test.add(new User(new Id(23L),"Szatan"));

        Map<Id, String> list = (Map<Id, String>) zadanie2(test);
        if(!list.containsKey(23)){
            System.out.println("nope");
        }
        System.out.println(list.get(new Id(23)));
        System.out.println(list.get(23));
        System.out.println(list);
    }


    //ZADANIE
    // Pan robot chciałby dostać hashmape z imieniem użytkownikaa jako wartościa i ich id jako kluczem
    // czy podolasz tej kolejnej próbie?



public static Map<Id, String> zadanie2(List<User> list) {
  Map<Id, String> mapa = list.stream()
          .collect(Collectors.toMap(User::getId, User::getName ));
  return mapa;
    }
    public static Object zadanie(List<User> list) {

        HashMap<Id, String> mapa = new HashMap<>();
for (int i = 0; i < list.size();){
    Id id = list.get(i).getId();
        String name = list.get(i).getName();
        mapa.put(id, name);
        i++;
}
        return mapa;
    }

}
