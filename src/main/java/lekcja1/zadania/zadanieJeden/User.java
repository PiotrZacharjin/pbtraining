package lekcja1.zadania.zadanieJeden;

public class User {
    private Id id;
    private String name;

    public User(Id id, String name) {
        this.id = id;
        this.name = name;
    }

    public Id getId() {
        return id;
    }

    public String getName() {
        return name;
    }
}
