package lekcja1.zadania.domowe.jeden.item;

import lekcja1.zadania.domowe.jeden.Npc;

public interface Item {

    ItemId getId();
    void onUse(Npc npc);
}
