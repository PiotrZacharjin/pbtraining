package lekcja1.zadania.domowe.jeden.item;

import lekcja1.zadania.domowe.jeden.Inventory;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

public class OrcInventory implements Inventory {
    public ArrayList<Item> orcBackpack;

    public OrcInventory(ArrayList<Item> playerBackpack) {
        this.orcBackpack = playerBackpack;
    }

    @Override
    public void add(Item item) {
        orcBackpack.add(item);
    }

    @Override

    public Optional<Item> get(ItemId searchedId) {

        Item found = null;
        for (Item item : orcBackpack) {
            if (Objects.equals(item.getId(), searchedId)) {
                found = item;

                System.out.println("Uzyto przedmiotu: " + searchedId + " Zostal on usuniety z twojego plecaka");
                break;
            }

        }

        if (found != null) {
            orcBackpack.remove(found);
        }

            return Optional.ofNullable(found);
        }

    }



