package lekcja1.zadania.domowe.jeden.item;

import lekcja1.zadania.domowe.jeden.Npc;

public class HealthPotion implements Item{


    @Override
    public ItemId getId() {
        return ItemId.HEALTHPOTION;
    }

    @Override
    public void onUse(Npc npc) {
npc.hp.healthIncrease(+2);
    }
}
