package lekcja1.zadania.domowe.jeden.item;

import lekcja1.zadania.domowe.jeden.Health;
import lekcja1.zadania.domowe.jeden.Inventory;
import lekcja1.zadania.domowe.jeden.Main;
import lekcja1.zadania.domowe.jeden.Npc;
import lekcja1.zadania.domowe.jeden.characters.Player;

import java.util.ArrayList;
import java.util.Objects;
import java.util.Optional;

public class PlayerInventory implements Inventory {
    public ArrayList<Item> playerBackpack;

    public PlayerInventory(ArrayList<Item> playerBackpack) {
        this.playerBackpack = playerBackpack;
    }
// Zmiana z konkretnego przedmiotu na (itemtype, item) a określenie przedmiotu w main podczas akcji

    @Override
    public void add(Item item) {
        playerBackpack.add(item);
    }

    @Override

    public Optional<Item> get(ItemId searchedId) {

        Item found = null;
        for (Item item : playerBackpack) {
            if (Objects.equals(item.getId(), searchedId)) {
                found = item;

                System.out.println("Uzyto przedmiotu: " + searchedId + " Zostal on usuniety z twojego plecaka");
                break;
            }

        }
        //sprawdzenie czy found null
        if (found != null) {
            playerBackpack.remove(found);
        }// Optional, owijanie obiektu
        else System.out.println("Brak podanego przedmiotu w ekwipunku");
            return Optional.ofNullable(found);
        }

    }



