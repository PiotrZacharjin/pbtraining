package lekcja1.zadania.domowe.jeden.item;

import lekcja1.zadania.domowe.jeden.Npc;

public class BerserkPotion implements Item{
    @Override
    public ItemId getId() {
        return ItemId.BERSERKPOTION;
    }

    @Override
    public void onUse(Npc npc) {
        Integer newAttackValue = npc.parameters.getValue() *4;

        npc.parameters.modify(newAttackValue);
    }
}
