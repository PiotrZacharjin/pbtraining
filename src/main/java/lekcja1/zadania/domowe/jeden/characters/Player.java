package lekcja1.zadania.domowe.jeden.characters;

import lekcja1.zadania.domowe.jeden.Health;
import lekcja1.zadania.domowe.jeden.Inventory;
import lekcja1.zadania.domowe.jeden.Npc;
import lekcja1.zadania.domowe.jeden.NpcAttackParameters;
import lekcja1.zadania.domowe.jeden.item.Item;
import lekcja1.zadania.domowe.jeden.item.ItemId;

import java.util.Optional;

public class Player extends Npc {


    public Player(Health hp, Inventory inventory, NpcAttackParameters parameters) {
        super(hp, inventory, parameters);
    }

    @Override
    protected void onInjure() {

if (hp.getValue() < 10) {

    Optional<Item> item = this.get(ItemId.HEALTHPOTION);
    item.ifPresent(item1 -> {
        item1.onUse(this);
        System.out.println("Bohater uzywa leczacej mikstury odnawiajac 2 HP");
    });
}

    }

  //  @Override
    public void hit(Npc npc) {
        super.hit(npc);

    }
}
