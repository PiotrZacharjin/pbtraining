package lekcja1.zadania.domowe.jeden.characters;

import lekcja1.zadania.domowe.jeden.*;
import lekcja1.zadania.domowe.jeden.item.Item;
import lekcja1.zadania.domowe.jeden.item.ItemId;

import java.util.Optional;

public class Orc extends Npc {
    public Orc(Health hp, Inventory inventory, NpcAttackParameters parameters) {
        super(hp, inventory, parameters);
    }

    @Override
    protected void onInjure() {
     if (hp.getValue() < 15) {
         Optional<Item> item = this.get(ItemId.BERSERKPOTION);
         item.ifPresent(item1 -> {
             item1.onUse(this);
             System.out.println("AAARRRGGhhhh, Po wypiciu eliksiru berserka Ork wydaje z siebie wrzask pelen grozy !");
             System.out.println("Zyskujac nad Orcze moce!");
             System.out.println("Sila ataku Orka wynosi teraz "+this.parameters.getValue());
         });
     }


    }


    public void hit(Npc npc) {
        super.hit(npc);
    }
}
