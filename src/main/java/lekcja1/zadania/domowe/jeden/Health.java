package lekcja1.zadania.domowe.jeden;

public class Health {

    private int value;
    private int max;

    public Health(int value, int max) {
        this.value = value;
        this.max = max;
    }

    public void healthDrop(int value){
        this.value -= value;
    }

    public void healthIncrease(int value){
        this.value += value;
    }

    public int getValue() {
        return value;
    }

    public int getMax() {
        return max;
    }
}
