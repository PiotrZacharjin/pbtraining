package lekcja1.zadania.domowe.jeden;


import lekcja1.zadania.domowe.jeden.characters.Orc;
import lekcja1.zadania.domowe.jeden.characters.Player;
import lekcja1.zadania.domowe.jeden.item.BerserkPotion;
import lekcja1.zadania.domowe.jeden.item.HealthPotion;
import lekcja1.zadania.domowe.jeden.item.OrcInventory;
import lekcja1.zadania.domowe.jeden.item.PlayerInventory;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        zadanie();

    }


    //ZADANIE
    // Pan robot ma kolege zajmujacego sie tworzeniem gier typu RPG
    // Potrzebuje żebyś pomógł mu z praca nad systemem walki dwoch postaci
    //Orka i człowieka
    // w tym rewolucyjnym systemie obie postacie obkłądaja sie nawzajem
    // postaci bija po 1 hp
    // postać bohatera dostaje 10 eliksirów zwracajacego hp na start
    // player ma 100hp  a orc 50hp
    // walka kończy się gdy któraś z postaci jest Dead
    // jeśli postać player spadnie do poniżej 10% hp to używa eliksiru leczacego zwracahacego 2hp
    // jesli postać orka spadnie poniżej 30% hp to bije 4x tyle co normalnie

    //każda postać ma mówić co robi System.out.println
    //pod koniec ma być ogłoszony zwyciesca
    public static void zadanie() {
        Player player = new Player(new Health(100, 100), new PlayerInventory(new ArrayList<>()), new NpcAttackParameters(1));
        Orc orc = new Orc(new Health(50, 50), new OrcInventory(new ArrayList<>()), new NpcAttackParameters(1));

        System.out.println("Po dwoch stronach arena staje naprzeciwko sobie dwojka dzielnych wojownikow, obydwoje wiedza ze arene opusci tylko jeden.  ");
        System.out.println("Ich ekwipunek zostaje uzupelniony o wczesniej wybrane przedmioty");
        playerEquipmentFill(player);
        orcEquipmentFill(orc);
        for (int checkResultOfBattle = 0; checkResultOfBattle < 1; ) {


            if (checkResultOfBattle == 0) {
                System.out.println("Bohater zadaje cios uderzajac za " + player.parameters.getValue() + " obrazen");
                player.hit(orc);
                System.out.println("Po uderzeniu Orkowi zostalo " + orc.hp.getValue() + " HP");
            }
            if (orc.hp.getValue() == 0) {
                checkResultOfBattle = 1;
                System.out.println("Walke wygral bohater zabijajac Orka.");
                break;
            }
            if (checkResultOfBattle == 0) {
                System.out.println("Ork zadaje cios uderzajac za " + orc.parameters.getValue() + " obrazen");
                orc.hit(player);
                System.out.println("Po uderzeniu Bohaterowi zostalo " + player.hp.getValue() + " HP");
            }
            if (player.hp.getValue() == 0) {
                checkResultOfBattle = 1;
                System.out.println("Walke wygral Ork zabijajac Bohatera.");
                break;
            }
        }
    }


    private static void playerEquipmentFill(Npc npc) {
        for (int i = 0; i < 10; i++) {
            npc.inventory.add(new HealthPotion());

        }
    }

    private static void orcEquipmentFill(Npc npc) {
        npc.inventory.add(new BerserkPotion());
    }
}
