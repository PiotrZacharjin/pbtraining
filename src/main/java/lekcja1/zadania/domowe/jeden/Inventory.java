package lekcja1.zadania.domowe.jeden;

import lekcja1.zadania.domowe.jeden.item.Item;
import lekcja1.zadania.domowe.jeden.item.ItemId;

import java.util.Optional;

public interface Inventory {

    void add(Item item);

    Optional<Item> get(ItemId itemId);
}
