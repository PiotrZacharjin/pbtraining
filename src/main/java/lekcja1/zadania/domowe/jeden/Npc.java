package lekcja1.zadania.domowe.jeden;

import lekcja1.zadania.domowe.jeden.item.Item;
import lekcja1.zadania.domowe.jeden.item.ItemId;

import java.util.Optional;

public abstract class Npc {
    public Health hp;
    public Inventory inventory;
    public NpcAttackParameters parameters;

    public Npc(Health hp, Inventory inventory, NpcAttackParameters parameters) {
        this.hp = hp;
        this.inventory = inventory;
        this.parameters = parameters;
    }

    protected abstract void onInjure();



    protected void hit(Npc npc){
// Do zmiany, ,musi pobierać wartość ataku z postacie ktora wywołuje funkcje , tymczasowo atk 1
//        Integer currentAttackValue =0;
//
//        int hitpoints = npc.parameters.getValue(currentAttackValue);
//        int hitpoints = -1;
//        npc.injure(hitpoints);
        Integer attack = parameters.getValue();
        int hitpoints = attack;
        npc.injure(hitpoints);
    }


    public void injure(int hitpoints) {

hp.healthDrop(hitpoints);
        onInjure();

    }

    protected Optional<Item> get(ItemId type){
        return inventory.get(type);
    }

    protected void use(Item item){
        item.onUse(this);
    }


}
