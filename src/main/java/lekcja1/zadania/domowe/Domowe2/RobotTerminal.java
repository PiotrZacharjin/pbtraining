package lekcja1.zadania.domowe.Domowe2;

import lekcja1.zadania.domowe.Domowe2.factory.QuestionComponents;
import lekcja1.zadania.domowe.Domowe2.factory.QuestionCreator;
import lekcja1.zadania.domowe.Domowe2.factory.QuestionValue;
import lekcja1.zadania.domowe.Domowe2.input.NameInputReader;
import lekcja1.zadania.domowe.Domowe2.input.QuestionInputReader;
import lekcja1.zadania.domowe.Domowe2.repozytorium.JsonPrinter;
import lekcja1.zadania.domowe.Domowe2.repozytorium.QuestionValuesRepository;

import java.util.List;


public class RobotTerminal {
    private NameInputReader reader;
    private QuestionInputReader questionReader;
    private QuestionComponents questionimport;
    private QuestionComponents nameImport;
    private QuestionCreator finalQuestionCreator;


    public void execute() {
        QuestionValuesRepository repository = new QuestionValuesRepository();
        JsonPrinter printer = new JsonPrinter();

        List<QuestionValue> baseRead = repository.findAll();
        // Stream filtrujący pytania od robota!!
//        List<QuestionValue> baseRead = repository.findAll().stream().filter((elementListy) -> {
//            return elementListy.getName().equals("Robot");
//        }).collect(Collectors.toList());

        System.out.println(baseRead);


        for (int i = 0; i < 2; i++) {
            QuestionValue data = new QuestionCreator().create();
            printer.printAsJson(data);
            repository.add(data);
            QuestionValue robotAnswer = new QuestionValue(":: HEllo World", "Robot", new QuestionComponents().currentDate());
            repository.add(robotAnswer);
            printer.printAsJson(robotAnswer);
        }
    }

}

