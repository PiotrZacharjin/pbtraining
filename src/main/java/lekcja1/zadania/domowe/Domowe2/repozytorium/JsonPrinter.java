package lekcja1.zadania.domowe.Domowe2.repozytorium;

import com.fasterxml.jackson.databind.ObjectMapper;
import lekcja1.zadania.domowe.Domowe2.factory.QuestionValue;
import lombok.SneakyThrows;

import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JsonPrinter {
    Path path = Paths.get("src/main/resources/Domowe2/JsonBase.txt");

    // Stworzone w JsonObjectCreator pytanie Zapisuje do Repozytorium do BAZY
//    public String jsonToBase() {
//        String finalJson = createJson();
//        //   saveToFile(finalJson);
//        return finalJson;
//    }


    @SneakyThrows
    public void printAsJson(QuestionValue data) {

        List<QuestionValue> listaObiektow = new ArrayList();
        listaObiektow.add(data);
        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listaObiektow);
        System.out.println(json);
    }


}
