package lekcja1.zadania.domowe.Domowe2.repozytorium;

import com.fasterxml.jackson.databind.ObjectMapper;
import lekcja1.zadania.domowe.Domowe2.factory.QuestionValue;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class JsonBaseReader {

    private Path path = Paths.get("src/main/resources/Domowe2/JsonBase.txt");


    @SneakyThrows
    public List<QuestionValue> jsonReader() {

        ObjectMapper mapper = new ObjectMapper();
        //  QuestionValues jsonToString = mapper.readValue(new File(String.valueOf(path)), QuestionValues.class);

        List<QuestionValue> jsonToString = mapper.readValue(new File("src/main/resources/Domowe2/JsonBase.txt"), ArrayList.class);
        return jsonToString;
    }

    // Działa, ale nie o to chodzi
    @SneakyThrows
    public String readFromFile() {
        {
            String jsonToString = Files.readString(path);
            return jsonToString;
        }


    }

}

