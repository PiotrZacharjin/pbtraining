package lekcja1.zadania.domowe.Domowe2.repozytorium;

import com.fasterxml.jackson.databind.ObjectMapper;
import lekcja1.zadania.domowe.Domowe2.factory.QuestionValue;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class QuestionValuesRepository {

    private Path path = Paths.get("src/main/resources/Domowe2/JsonBase.txt");

    @SneakyThrows
    public void add(QuestionValue data) {
        checkFile();
        List<QuestionValue> listaObiektow = readQuestions();
        listaObiektow.add(data);
        ObjectMapper mapper = new ObjectMapper();
        saveToFile(mapper.writerWithDefaultPrettyPrinter().writeValueAsString(listaObiektow));

    }

    public List<QuestionValue> findAll() {
        checkFile();
        return readQuestions();

    }

    @SneakyThrows
    private List<QuestionValue> readQuestions() {

        ObjectMapper mapper = new ObjectMapper();


        List<QuestionValue> questionList = mapper.readValue(new File("src/main/resources/Domowe2/JsonBase.txt"), ArrayList.class);
        return questionList;
    }

    @SneakyThrows
    private void checkFile() {
        File f = new File("src/main/resources/Domowe2/JsonBase.txt");

        if (f.exists() && !f.isDirectory()) {
            if (f.length() == 0) {
                saveBlankTemplate();
            }

        } else {
            f.createNewFile();
            saveBlankTemplate();
        }
    }

    @SneakyThrows
    private void saveBlankTemplate() {
        Path fileName = Path.of("src/main/resources/Domowe2/JsonBase.txt");
        String content = "[ {\n" +
                "  \"name\" : \"Template\",\n" +
                "  \"date\" : \"0000-00-00 00:00:00.001\",\n" +
                "  \"question\" : \"No Questions \"\n" +
                "}]";
        Files.writeString(fileName, content);
    }


    @SneakyThrows
    private void saveToFile(String JsonStringToFile) {


        Files.writeString(path, JsonStringToFile, StandardOpenOption.WRITE);


    }


}
