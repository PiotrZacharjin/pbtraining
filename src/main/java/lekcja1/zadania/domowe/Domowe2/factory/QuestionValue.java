package lekcja1.zadania.domowe.Domowe2.factory;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/// Puki co bez sensu, do przemyślenia czy zostanie, z założenia powinno mieć w sobie data Stamp i imie
//stampa można dodać w postprodukcji


@Data
@NoArgsConstructor
@AllArgsConstructor
public class QuestionValue {


    private String Question;
    private String name;
    private String date;
}
