package lekcja1.zadania.domowe.Domowe2.factory;

public class QuestionCreator {


    public QuestionValue create() {

        QuestionValue finalQuestion = new QuestionValue(createQuestion(), createName(), createDate());
        return finalQuestion;
    }


    private String createQuestion() {
        return new QuestionComponents().readQuestion();
    }

    private String createName() {
        return new QuestionComponents().readName();
    }

    private String createDate() {
        return new QuestionComponents().currentDate();
    }
}