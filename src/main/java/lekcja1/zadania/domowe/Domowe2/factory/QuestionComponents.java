package lekcja1.zadania.domowe.Domowe2.factory;

import lekcja1.zadania.domowe.Domowe2.input.NameInputReader;
import lekcja1.zadania.domowe.Domowe2.input.QuestionInputReader;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;

public class QuestionComponents {
    //@@@@@@@PYTANIE@@@@@@@@@@@
    //Do czego odnośi się sdf
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd");

    public String currentDate() {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        return String.valueOf(timestamp);
    }

    public String readQuestion() {

        String questionImporter = new QuestionInputReader().read();
        return questionImporter;
    }

    public String readName() {
        String nameImporter = new NameInputReader().read();
        return nameImporter;
    }
}
