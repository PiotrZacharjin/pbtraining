package lekcja1.zadania.domowe.dwa.terminal;

import lekcja1.zadania.domowe.dwa.Id;
import lekcja1.zadania.domowe.dwa.UserService;
import lekcja1.zadania.domowe.dwa.terminal.command.Command;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandDTO;
import lekcja1.zadania.domowe.dwa.user.UserManagementService;
import lekcja1.zadania.zadanieCztery.UserDataDto;

public class AddUserFactory {
    private UserService service;

    public AddUserFactory(UserService service) {
        this.service = service;
    }


    public Command create(CommandDTO command){
        String username = command.getProperty("username").getValue();
        String password = command.getProperty("password").getValue();
        String userId = String.valueOf(username.hashCode());
        UserDataDto dto = new UserDataDto(userId, username,password);

        return ()->{
            service = new UserManagementService();
            Id id = service.addUser(dto);
            System.out.println("Added user "+username+id);
        };
    }
}
