package lekcja1.zadania.domowe.dwa.terminal.property;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.Collectors;

public class PropertiesFactory {


    public Map<String, PropertyDto> create(String command) {
        return Arrays.stream(command.split(" "))
                .filter((text) -> text.contains("="))
                .map(text -> {
                    text = text.replace("--","");
                    String[] values = text.split("=");
                    return new PropertyDto(values[0], values[1]);
                })
                .collect(Collectors.toMap(PropertyDto::getName, p -> p));
    }
}
