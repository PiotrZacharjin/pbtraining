package lekcja1.zadania.domowe.dwa.terminal;

import java.io.BufferedReader;
import java.io.IOException;

public class CommandInputReader {
    private BufferedReader reader;


    public CommandInputReader(BufferedReader scanner) {
        this.reader = scanner;
    }

    String read(){
            try {
                String s = reader.readLine();
                return s;
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
    }
}
