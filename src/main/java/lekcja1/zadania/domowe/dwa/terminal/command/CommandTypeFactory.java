package lekcja1.zadania.domowe.dwa.terminal.command;

public class CommandTypeFactory {

    public CommandType create(String command){
        if(command.startsWith("user add")){
            return CommandType.ADD;
        }

        if(command.startsWith("user remove")){
            return CommandType.REMOVE;
        }

        if(command.startsWith("user list")){
            return CommandType.LIST;
        }

        if(command.startsWith("exit")){
            return CommandType.EXIT;
        }
        return CommandType.UNIDENTIFIED;
    }
}
