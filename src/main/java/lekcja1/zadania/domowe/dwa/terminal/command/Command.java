package lekcja1.zadania.domowe.dwa.terminal.command;

public interface Command {
    void execute();
}
