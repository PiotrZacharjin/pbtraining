package lekcja1.zadania.domowe.dwa.terminal;

import java.io.BufferedReader;
import java.io.InputStreamReader;

public class TerminalReaderFactory {


    public static CommandInputReader create(){
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        return new CommandInputReader(bufferedReader);
    }
}
