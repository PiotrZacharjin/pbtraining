package lekcja1.zadania.domowe.dwa.terminal;

import lekcja1.zadania.domowe.dwa.terminal.command.Command;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandDTO;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandDTOFactory;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandFactory;

public class Terminal {

    private CommandFactory commandFactory;
    private CommandDTOFactory dtoFactory;
    private CommandInputReader reader;

    public Terminal(CommandFactory commandFactory, CommandDTOFactory dtoFactory, CommandInputReader reader) {
        this.commandFactory = commandFactory;
        this.dtoFactory = dtoFactory;
        this.reader = reader;
    }


    public void start() {
        System.out.println("User Registration System v0.1");
        while (true){
            System.out.println(">");
            String commandText = reader.read();
            CommandDTO commandDTO = dtoFactory.create(commandText);
            Command command = commandFactory.create(commandDTO);
            command.execute();
        }
    }


}
