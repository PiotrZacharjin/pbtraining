package lekcja1.zadania.domowe.dwa.terminal.command;

import lekcja1.zadania.domowe.dwa.terminal.property.PropertiesFactory;
import lekcja1.zadania.domowe.dwa.terminal.property.PropertyDto;

import java.util.Map;

public class CommandDTOFactory {

    private CommandTypeFactory typeFactory;
    private PropertiesFactory propertiesFactory;

    public CommandDTOFactory(CommandTypeFactory typeFactory, PropertiesFactory propertiesFactory) {
        this.typeFactory = typeFactory;
        this.propertiesFactory = propertiesFactory;
    }

    public CommandDTO create(String command){
        CommandType commandType = typeFactory.create(command);
        Map<String, PropertyDto> properties = propertiesFactory.create(command);
        return new CommandDTO(commandType, properties);
    }
}
