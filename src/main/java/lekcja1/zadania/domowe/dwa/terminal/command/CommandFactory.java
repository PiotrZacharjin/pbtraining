package lekcja1.zadania.domowe.dwa.terminal.command;

import lekcja1.zadania.domowe.dwa.UserService;
import lekcja1.zadania.domowe.dwa.terminal.AddUserFactory;
import lekcja1.zadania.domowe.dwa.terminal.ListUsersFactory;
import lekcja1.zadania.domowe.dwa.terminal.RemoveUserFactory;

public class CommandFactory {
    private UserService service;

    public CommandFactory(UserService service) {
        this.service = service;
    }

    public Command create(CommandDTO command) {
        CommandType type = command.getType();
        switch (type){
            case ADD:
                return new AddUserFactory(service).create(command);
            case REMOVE:
                return new RemoveUserFactory(service).create(command);
            case LIST:
                return new ListUsersFactory(service).create();
            case EXIT:
                return ()->{throw new RuntimeException("user exited");};
            default:
                return ()->{
                    System.out.println("Command "+command+" doesn't exist");
                };
        }
    }
}
