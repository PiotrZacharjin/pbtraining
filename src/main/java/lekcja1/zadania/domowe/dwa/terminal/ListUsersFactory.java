package lekcja1.zadania.domowe.dwa.terminal;

import lekcja1.zadania.domowe.dwa.UserService;
import lekcja1.zadania.domowe.dwa.terminal.command.Command;
import lekcja1.zadania.domowe.dwa.user.UserManagementService;

public class ListUsersFactory {
    private UserService service;

    public ListUsersFactory(UserService service) {
        this.service = service;
    }


    public Command create(){
        return ()->{
            service = new UserManagementService();
            System.out.println(service.listUsers());
        };
    }
}
