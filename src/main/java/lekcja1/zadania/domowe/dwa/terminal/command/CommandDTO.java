package lekcja1.zadania.domowe.dwa.terminal.command;

import lekcja1.zadania.domowe.dwa.terminal.property.PropertyDto;

import java.util.Map;

public class CommandDTO {
    private CommandType type;
    private Map<String, PropertyDto> property;

    public CommandDTO(CommandType type, Map<String, PropertyDto> property) {
        this.type = type;
        this.property = property;
    }

    public CommandType getType() {
        return type;
    }

    public PropertyDto getProperty(String property) {
        return this.property.get(property);
    }
}
