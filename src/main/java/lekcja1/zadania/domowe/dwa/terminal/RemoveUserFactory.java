package lekcja1.zadania.domowe.dwa.terminal;

import lekcja1.zadania.domowe.dwa.Id;
import lekcja1.zadania.domowe.dwa.UserService;
import lekcja1.zadania.domowe.dwa.terminal.command.Command;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandDTO;
import lekcja1.zadania.domowe.dwa.user.UserManagementService;

public class RemoveUserFactory {
    private UserService service;

    public RemoveUserFactory(UserService service) {
        this.service = service;
    }


    public Command create(CommandDTO command){
        String idText = command.getProperty("id").getValue();
      String idText2 = idText.replace("{", "");
       String idText3 = idText2.replace("}", "");
        return ()->{
            service = new UserManagementService();
            Id id =new Id(Long.parseLong(idText3));
            service.removeUser(id);
            System.out.println("Removed user "+id);
        };
    }
}
