package lekcja1.zadania.domowe.dwa.terminal.property;

public class PropertyDto {

    private String value;
    private String name;

    public PropertyDto(String name, String value) {
        this.value = value;
        this.name = name;
    }

    public String getValue() {
        return value;
    }

    public String getName() {
        return name;
    }
}
