package lekcja1.zadania.domowe.dwa;


import lekcja1.zadania.domowe.dwa.terminal.Terminal;
import lekcja1.zadania.domowe.dwa.terminal.TerminalReaderFactory;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandDTOFactory;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandFactory;
import lekcja1.zadania.domowe.dwa.terminal.command.CommandTypeFactory;
import lekcja1.zadania.domowe.dwa.terminal.property.PropertiesFactory;

public class Main {

    public static void main(String[] args) {
        new Terminal(
                new CommandFactory(zadanie()),
                new CommandDTOFactory(new CommandTypeFactory(), new PropertiesFactory()),
                TerminalReaderFactory.create()
        ).start();
    }


    //ZADANIE
    // Pan robot jest pod wrażeniem twoich poczynań
    // Jako najtańsza siła robocza włąsnie wygrałeś przetarg na system do rejestracji użytkowników
    // FULL FREESTYLE
    // Użyj nabytej wiedzy żeby zróbić system który:
    //      rejestruje Użytkowników
    //      wyświetla Zarejestrowanych Użytkowników
    //      Ma możliwość kasowania uzytkowników po ID

    // komunikacja z twoim serwisem bedzie odbywać się przez terminal
    // zostało to już zaimplementowane
    // komendy sa nastepujace:
    // user add --username={username} --password={password}
    // user remove --id={id}
    // user list
    // exit
    // piszesz komende i w terminalu naciskasz enter

    public static UserService zadanie() {
        return null;
    }

}
