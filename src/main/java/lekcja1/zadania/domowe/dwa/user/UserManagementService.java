package lekcja1.zadania.domowe.dwa.user;

import lekcja1.zadania.domowe.dwa.Id;
import lekcja1.zadania.domowe.dwa.UserService;
import lekcja1.zadania.zadanieCztery.UserDataDto;

import java.util.ArrayList;
import java.util.List;

public class UserManagementService implements UserService {

    @Override
    public Id addUser(UserDataDto userDataDto) {

        return new UserAddingService().execute(userDataDto);
    }

    @Override
    public void removeUser(Id id) {
new UserRemoveService().execute(id);
    }

    @Override
    public ArrayList<String> listUsers() {
        ArrayList<String> usersList = new UserListService().execute();


        return usersList;
    }
}
