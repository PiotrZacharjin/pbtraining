package lekcja1.zadania.domowe.dwa.user;

import lekcja1.zadania.domowe.dwa.Id;
import lekcja1.zadania.zadanieCztery.UserDataDto;


import java.util.ArrayList;

public class UserRemoveService {


    public void execute(Id id){
        ArrayList<UserDataDto> userBase = new UserListRepository().load();
        removeUser(userBase, id);
        new UserListRepository().save(userBase);

    }

    private void removeUser(ArrayList<UserDataDto> userBase, Id id) {
        for (UserDataDto base:userBase) {
            if(base.getId().equals(String.valueOf(id.getId()))){
                System.out.println("User ID:"+base.getId()+" has been removed.");
                userBase.remove(base);
                break;
            }
        }



    }


}
