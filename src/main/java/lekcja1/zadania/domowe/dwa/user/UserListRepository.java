package lekcja1.zadania.domowe.dwa.user;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lekcja1.zadania.zadanieCztery.UserDataDto;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;


public class UserListRepository {


    public ArrayList<UserDataDto> load() {
      ObjectMapper objectMapper = new ObjectMapper();
        ArrayList<UserDataDto> dataList = null;
        try {
            dataList = objectMapper.<ArrayList<UserDataDto>>readValue(
                    new File("src/main/java/lekcja1/zadania/domowe/dwa/user/userBase.txt"),
                    new TypeReference<ArrayList<UserDataDto>>() {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return dataList;
    }


    public void save(List<UserDataDto> userList){
        String userListAsJson =  listToJson(userList);
        try {
            String fileCleaner = "";
            Files.writeString(Paths.get("src/main/java/lekcja1/zadania/domowe/dwa/user/userBase.txt"), fileCleaner, StandardOpenOption.CREATE);
            Files.writeString(Paths.get("src/main/java/lekcja1/zadania/domowe/dwa/user/userBase.txt"), userListAsJson, StandardOpenOption.WRITE);
        } catch (IOException exception) {
            exception.printStackTrace();
        }

    }

    private String listToJson(List<UserDataDto>userList){
        ObjectMapper objectMapper = new ObjectMapper();
        String userListAsJson = "";
        try {
            userListAsJson = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(userList);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return userListAsJson;
    }
}
