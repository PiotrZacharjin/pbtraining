package lekcja1.zadania.domowe.dwa.user;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lekcja1.zadania.domowe.dwa.Id;
import lekcja1.zadania.zadanieCztery.UserDataDto;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

public class userCopy {
    ObjectMapper mapper = new ObjectMapper();
    public List<UserDataDto> loadFromJson() {

        String base = "";

        try {
            base = Files.readString(Paths.get("src/main/java/lekcja1/zadania/domowe/dwa/user/userBase.txt"));
            if (base.length() == 0) {
                base = userListTemplate();
            }
        } catch (IOException exception) {
            exception.printStackTrace();
        }
        List<UserDataDto> result = new ArrayList<>();
        try {
            result = new ObjectMapper().readValue(new File("src/main/java/lekcja1/zadania/domowe/dwa/user/userBase.txt"),
                    new TypeReference<List<UserDataDto>>() {
                    });
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
    private String userListTemplate(){
        String template ="[ {\n" +
                "  \"username\" : \"{blank}\",\n" +
                "  \"password\" : \"{blank}\",\n" +
                "  \"id\" : {\n" +
                "    \"id\" : 402199012\n" +
                "  }\n" +
                "} ]";
        return template;
    }
}
