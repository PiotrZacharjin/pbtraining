package lekcja1.zadania.domowe.dwa;

import lekcja1.zadania.zadanieCztery.UserDataDto;

import java.util.List;

public interface UserService {

    Id addUser(UserDataDto userDataDto);

    void removeUser(Id id);

    List<String> listUsers();
}
