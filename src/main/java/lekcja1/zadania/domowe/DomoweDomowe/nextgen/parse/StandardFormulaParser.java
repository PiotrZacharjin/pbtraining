package lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse;

import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.components.FormulaComponent;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.components.FormulaComponentFactory;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.operator.FormulaOperator;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.operator.FormulaOperatorFactory;
import lombok.AllArgsConstructor;

// OBIEKT ODPOWIEDZIALNY ZA ROZPISANIE POZYSKANYCH WARTOSCI DO ZMIENNYCH Z DTO


@AllArgsConstructor
public class StandardFormulaParser implements InputParser {


    @Override
    public FormulaDTO parse(String input) {
        FormulaComponent components = createComponents(input);
        FormulaOperator operator = createOperator(input);
        return new FormulaDTO(
                components.getA(),
                operator,
                components.getB());
    }

    private FormulaOperator createOperator(String text) {
        return new FormulaOperatorFactory().createOperator(text);
    }

    private FormulaComponent createComponents(String text) {
        return new FormulaComponentFactory().create(text);
    }

    // ŹLE DZIALA DO POPRAWY PO! PRZENISIENIU DO INNEJ KLASY
    private boolean assertTrue(boolean matches) {
        return matches;
    }

    // Do przenisienia od oddzielnej klasy i poprawy, zle dziala...
    boolean operatorChecker(String operatorTester) {

        assertTrue(operatorTester.matches("(.*)[+],[-],[*],[/](.*)"));
        if (assertTrue(operatorTester.matches(String.valueOf(true)))) {
            return true;
        }
        return false;
    }

    // DO PRZENIESIENIA DO ODDZIELNEJ KLASY!
    boolean numberChecker(String abNumberTester) {

        String[] dzielenieRownania = abNumberTester.split("[-]|[+]|[*]|[/]");
        String ax = dzielenieRownania[0];
        String bx = dzielenieRownania[1];
        if (ax == null || bx == null) {
            return false;
        }
        try {
            double d = Double.parseDouble(ax);
        } catch (NumberFormatException numberFormatException) {
            return false;
        }
        try {
            double d = Double.parseDouble(bx);
        } catch (NumberFormatException numberFormatException) {
            return false;
        }
        return true;
    }


}
