package lekcja1.zadania.domowe.DomoweDomowe.nextgen.result;

import com.opencsv.CSVWriter;
import lombok.SneakyThrows;

import java.io.FileWriter;
import java.io.IOException;

public class CsvFileWriter implements ResultWriter {
    @SneakyThrows
    @Override
    public void write(FormulaResult result) {

        //    csvWriterAll(result);
        String resultAsString = String.valueOf(result);
        csvWriterDwa(resultAsString);
    }

    private void csvWriterDwa(String toCsv) throws IOException {
        CSVWriter writer = new CSVWriter(new FileWriter("yourfile.csv"), '\t');

        writer.writeNext(new String[]{toCsv});
        writer.close();
    }


//
//    public String csvWriterAll(List<String[]> stringArray, Path path) throws Exception {
//        CSVWriter writer = new CSVWriter(new FileWriter(path.toString()));
//        writer.writeAll(stringArray);
//        writer.close();
//        return readFile(path);
//    }
//    public String csvWriterAll() throws Exception {
//        Path path = Paths.get(
//                ClassLoader.getSystemResource("csv/writtenAll.csv").toURI());
//        return CsvWriterExamples.csvWriterAll(Helpers.fourColumnCsvString(), path);
//    }
}
