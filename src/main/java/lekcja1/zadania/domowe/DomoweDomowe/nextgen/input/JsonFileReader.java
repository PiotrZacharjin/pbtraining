package lekcja1.zadania.domowe.DomoweDomowe.nextgen.input;

import com.fasterxml.jackson.databind.ObjectMapper;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.FormulaDTO;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.PrettyFormulaCreator;
import lombok.SneakyThrows;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;

public class JsonFileReader implements TerminalInputReader {

    private Path path;


    @SneakyThrows
    @Override
    public String read() {


        //    Wybrać i przetestować jedną z metod, czy będzie działać.
        //Bez sensu, bo wrzuci mi wszytgko jako String, razem z nazwami zmiennych, st[przetextować
        //  readFromFile();
        // Założenie, zwraca nam 3 wartości z formulaDTO, do późniejszej obróbki, hmm, zobaczymy
        //Ttylko Read jest stały, więc nie zrobi mi returna.
        //Ew. zwraca w postacie obiektu, któy trafia do Parsera i dopiero wypluwa return Stringa.

        FormulaDTO formula = jsonToObject();
        return new PrettyFormulaCreator().create(formula);
    }

    @SneakyThrows
    private FormulaDTO jsonToObject() {
        ObjectMapper mapper = new ObjectMapper();
        FormulaDTO formulaFromJasonFile = mapper.readValue(new File(String.valueOf(path)), FormulaDTO.class);

        return formulaFromJasonFile;

    }

    @SneakyThrows
    private void readFromFile() {
        {
            String jsonToString = Files.readString(path);

        }

    }


}
