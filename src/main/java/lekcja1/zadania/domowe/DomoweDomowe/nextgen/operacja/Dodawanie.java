package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Dodawanie implements Operacja {
    private Double a;
    private Double b;

    @Override
    public Double przeprowadzOperacje() {
        return a+b;
    }
}
