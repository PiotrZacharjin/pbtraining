package lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.operator;

public class FormulaOperatorFactory {

    public FormulaOperator createOperator(String znakOperatora) {
        String[] dzielenieRownania = znakOperatora.split("[-]|[+]|[*]|[/]");
        String testZnaku = dzielenieRownania[1];
        Integer dlugoscRownania = znakOperatora.length();
        Integer dlugoscLiczby2 = Integer.valueOf(testZnaku.length());
        Integer miejsceZnaku = (dlugoscRownania - dlugoscLiczby2) - 1;
        String operator = String.valueOf(znakOperatora.charAt(miejsceZnaku));
        return new FormulaOperator(operator);
    }

}
