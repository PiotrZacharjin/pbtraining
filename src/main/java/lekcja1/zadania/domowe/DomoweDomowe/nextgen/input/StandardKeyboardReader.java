package lekcja1.zadania.domowe.DomoweDomowe.nextgen.input;

import java.util.Scanner;

public class StandardKeyboardReader implements TerminalInputReader {

    // czytanie z klawiatury, return zwraca stringa z zczytaną wartością


    @Override
    public String read() {
        Scanner input = new Scanner(System.in);
        String readResult = input.nextLine();

        return readResult;
    }
}
