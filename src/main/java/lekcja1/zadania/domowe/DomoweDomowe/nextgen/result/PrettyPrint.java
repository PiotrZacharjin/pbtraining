package lekcja1.zadania.domowe.DomoweDomowe.nextgen.result;

import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.FormulaDTO;

public class PrettyPrint implements ResultWriter {
    @Override
    public void write(FormulaResult result) {
        // String text = new FormulaResult.toString.Converter().convert(result);
        // String text = String.valueOf(result);

        FormulaDTO formula = result.getFormula();

        String resultAsString = formula.getA().toString() + formula.getOperator().getOperator() + formula.getB() + '=' + result.getResult();
        System.out.println(resultAsString);
    }
    // Dodanie translacji result do Stringa


}
// FormulaResultToStringConverter().convert(result);