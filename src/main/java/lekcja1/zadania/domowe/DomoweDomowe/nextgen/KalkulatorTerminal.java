package lekcja1.zadania.domowe.DomoweDomowe.nextgen;

import lekcja1.zadania.domowe.Domowe2.input.RobotTerminalInputReader;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja.Operacja;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja.OperacjaFabryka;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.FormulaDTO;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.InputParser;
import lekcja1.zadania.domowe.DomoweDomowe.nextgen.result.ResultWriter;
import lombok.AllArgsConstructor;
//ZADANIE

//dopisanie Pakietu Resultwriter
@AllArgsConstructor
public class KalkulatorTerminal {

    private RobotTerminalInputReader reader;
    private InputParser parser;
    private ResultWriter writer;

    public void execute() {
        while (true) {
            String read = reader.read();
            FormulaDTO formula = parser.parse(read);
            OperacjaFabryka factory = new OperacjaFabryka(formula.getA(), formula.getOperator(), formula.getB());
            Operacja operacja = factory.stworz(); //przesyłanie do ResultWriter!
        }

    }
}
