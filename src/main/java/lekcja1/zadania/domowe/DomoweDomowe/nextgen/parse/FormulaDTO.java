package lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse;

import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.operator.FormulaOperator;
import lombok.Value;

@Value
public class FormulaDTO {
    private Double a;
    private FormulaOperator operator;
    private Double b;
}
