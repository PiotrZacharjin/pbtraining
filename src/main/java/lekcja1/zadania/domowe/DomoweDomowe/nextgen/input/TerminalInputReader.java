package lekcja1.zadania.domowe.DomoweDomowe.nextgen.input;
//ODPOWIEDZIALNE ZA ZCZYTYWANIE Z EKRANU/PLIKU!

//Stworz 2 implementacje Readera
// 3 obiekty implementujace Terminal Input Reader

//- czytaj z klawiatury : StandardKeayboardReader
//- czytaj z pliku <- tu moglbys zrobic test (plik do testow w resource) : StandardFileReader
//
//  new BufferedReader(new InputStreamReader(name))
//                .lines().collect(Collectors.joining("\n"));
//- zwracaj staly string
public interface TerminalInputReader {

    String read();

}
