package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.operator.FormulaOperator;
import lombok.AllArgsConstructor;

@AllArgsConstructor
public class OperacjaFabryka {
    private Double a;
    private FormulaOperator operator;
    private Double b;

    public Operacja stworz() {
        if (operator.equals("+")) {
            return new Dodawanie(a, b);
        }
        if (operator.equals("-")) {
            return new Odejmowanie(a, b);
        }
        if (operator.equals("*")) {
            return new Mnozenie(a, b);
        }
        if (operator.equals("/")) {
            return new Dzielenie(a, b);
        }
        return null;
    }
}
