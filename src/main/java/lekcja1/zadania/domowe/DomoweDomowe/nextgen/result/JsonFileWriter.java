package lekcja1.zadania.domowe.DomoweDomowe.nextgen.result;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;

import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;

@AllArgsConstructor
public class JsonFileWriter implements ResultWriter {

    private Path path;


    @SneakyThrows
    @Override
    public void write(FormulaResult result) {

        String json = createJson(result);
        saveToFile(json);

    }

    @SneakyThrows
    private String createJson(FormulaResult result) {
        ObjectMapper mapper = new ObjectMapper();
        // return mapper.writeValueAsString(result);
        return mapper.writerWithDefaultPrettyPrinter().writeValueAsString(result);
    }

    // hmm,


    @SneakyThrows
    private void saveToFile(String JsonStringToFile) {

        Files.writeString(path, JsonStringToFile, StandardOpenOption.CREATE);


    }
}
