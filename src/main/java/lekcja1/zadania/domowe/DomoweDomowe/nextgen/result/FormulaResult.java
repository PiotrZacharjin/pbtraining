package lekcja1.zadania.domowe.DomoweDomowe.nextgen.result;

import lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.FormulaDTO;
import lombok.Value;

@Value
public class FormulaResult {
    private FormulaDTO formula;
    private Double result;
}
