package lekcja1.zadania.domowe.DomoweDomowe.nextgen.parse.components;

public class FormulaComponentFactory {

    public FormulaComponent create(String text) {
        String[] dzielenieRownania = text.split("[-]|[+]|[*]|[/]");
        Double a = Double.valueOf(dzielenieRownania[0]);
        Double b = Double.valueOf(dzielenieRownania[1]);
        return new FormulaComponent(a, b);
    }

}
