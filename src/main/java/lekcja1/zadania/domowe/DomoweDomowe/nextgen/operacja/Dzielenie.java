package lekcja1.zadania.domowe.DomoweDomowe.nextgen.operacja;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Dzielenie implements Operacja {
    private Double a;
    private Double b;


    @Override
    public Double przeprowadzOperacje() {
        if (a == 0 || b == 0) {
            System.out.println("Nie można dzielić przez 0, Cholero");
            return null;
        } else return a / b;

    }
}