package lekcja1.zadania.domowe.DomoweDomowe.nextgen.input;

import lombok.AllArgsConstructor;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Collectors;

@AllArgsConstructor
public class StandardFileReader implements TerminalInputReader {

    private InputStream inputStream;


    @Override
    public String read() {
        String readResult = new BufferedReader(new InputStreamReader(inputStream))
                .lines().collect(Collectors.joining());

        return readResult;
    }
}
