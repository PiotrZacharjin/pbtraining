package lekcja1.zadania.zadanieTrzy;


public class Main {

    public static void main(String[] args) {
        Robot robot = new Robot(zadanie());
        new CalculatorValidator(robot).validate();
        System.out.println(robot);
    }


    //ZADANIE
    // Pan robot potrzebuje kalkulatora do pracy
    // outsourcował stworzenie kalkulatora do indii ale spartaczyli robote
    // i teraz potrzebuje pomocy w dokończeniu kodu
    // Pan Robot sprawdzi Twój kalkulator czy działa (Calculator Validator)
    // Ciebie interesuje przedewszystkim OperationFactory.
    // Zobacz kod rozkmiń jak działa i dajesz :D
    public static Calculator zadanie() {
       return new Calculator(new OperationFactory());
    }

}
