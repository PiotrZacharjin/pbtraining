package lekcja1.zadania.zadanieTrzy;

public class Calculator {

    private OperationFactory operationFactory;

    public Calculator(OperationFactory operationFactory) {
        this.operationFactory = operationFactory;
    }

    Number calculate(OperationType type, Number a, Number b){
        Operation operation = operationFactory.create(type);
        return operation.execute(a,b);
    }
}
