package lekcja1.zadania.zadanieTrzy;

public class CalculatorValidator {
    private Robot robot;

    public CalculatorValidator(Robot robot) {
        this.robot = robot;
    }


    public void validate() {
        Number sum = robot.sum(4, 10);

        if(sum.intValue() != 14){
            robot.escapeFromEarth();
        }
        Number subtraction = robot.subtract(9, 3);

        if(subtraction.intValue() != 6){
            robot.escapeFromEarth();
        }

        Number division = robot.divide(12, 3);
        if(division.intValue() != 4){
            robot.escapeFromEarth();
        }

        Number multiplication = robot.multiple(12, 3);
        if(multiplication.intValue() != 36){
            robot.escapeFromEarth();
        }
    }
}
