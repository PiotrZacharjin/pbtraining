package lekcja1.zadania.zadanieTrzy;

public class Robot {

    private Calculator calculator;

    public Robot(Calculator calculator) {
        this.calculator = calculator;
    }


    Number sum(Number a, Number b){
        return calculator.calculate(OperationType.SUM, a,b);
    }

    Number subtract(Number a, Number b){
        return calculator.calculate(OperationType.SUBTRACT, a,b);
    }

    Number divide(Number a, Number b){
        return calculator.calculate(OperationType.DIVIDE, a,b);
    }

    Number multiple(Number a, Number b){
        return calculator.calculate(OperationType.MULTIPLE, a,b);
    }


    @Override
    public String toString() {
        return "Hurra udało sie" ;
    }

    public void escapeFromEarth() {
        throw new RuntimeException("Robot spierdolił z planety ziemi. Try again");
    }

}
