package lekcja1.zadania.zadanieTrzy.operacje;

import lekcja1.zadania.zadanieTrzy.Operation;

public class Divide implements Operation {
    @Override
    public Number execute(Number a, Number b) {
        Integer ad = a.intValue();
        Integer bd = b.intValue();

        return ad/bd;

    }
}
