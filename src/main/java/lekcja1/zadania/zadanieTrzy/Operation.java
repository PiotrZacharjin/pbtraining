package lekcja1.zadania.zadanieTrzy;

public interface Operation {

    Number execute(Number a, Number b);
}
