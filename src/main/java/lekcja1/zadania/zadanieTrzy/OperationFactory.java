package lekcja1.zadania.zadanieTrzy;

import lekcja1.zadania.zadanieTrzy.operacje.Divide;
import lekcja1.zadania.zadanieTrzy.operacje.Multiple;
import lekcja1.zadania.zadanieTrzy.operacje.Subtract;
import lekcja1.zadania.zadanieTrzy.operacje.Sum;


class OperationFactory {

    Operation create(OperationType type) {
        switch (type) {
            case SUM:
                return new Sum();
            case SUBTRACT:
                return new Subtract();
            case DIVIDE:
                return new Divide();
            case MULTIPLE:
                return new Multiple();
            default:
                return null;
        }
    }


}
