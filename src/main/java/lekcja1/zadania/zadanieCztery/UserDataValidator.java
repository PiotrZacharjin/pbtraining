package lekcja1.zadania.zadanieCztery;

public class UserDataValidator {


    private UsernameValidator usernameValidator;
    private PasswordValidator passwordValidator;

    public UserDataValidator(UsernameValidator usernameValidator, PasswordValidator passwordValidator) {
        this.usernameValidator = usernameValidator;
        this.passwordValidator = passwordValidator;
    }

    UserDataValidationResult validate(UserDataDto dto){
        boolean usernameValid = usernameValidator
                .validate(dto.getUsername());
        boolean passwordValid = passwordValidator
                .validate(dto.getPassword());
        return new UserDataValidationResult(usernameValid,passwordValid);
    }
}
