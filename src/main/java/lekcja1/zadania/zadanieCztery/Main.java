package lekcja1.zadania.zadanieCztery;


public class Main {

    public static void main(String[] args) {
        UserDataValidator validator = zadanie();

        UserDataDto userData = new UserDataDto("bolek?", "lol");
        UserDataValidationResult result = validator.validate(userData);

        if(result.isPasswordValid()){
            throw new RuntimeException("this password should not be valid");
        }

        if(result.isUsernameValid()){
            throw new RuntimeException("this username should not be valid");

        }

    }


    //ZADANIE
    // Pan robot planuje zrobić system do rejestracji użytkowników
    // Z racji że udało Ci sie uratować projekt kalkulatora to
    // stwierdza że podołasz i temu zadaniu

    //potrzebuje być zaimplementował brakujący element układanki:
    //potrzebuje zaimplementować obiekt walidujący informacje o użytkowniku

    //czy username nie zawiera zapytajnika ?
    //czy username nie zawiera textu bolek
    //czy hasło ma:
    //  wiecej znaków niż 6
    //  chociaz jedna cyfra

    //do sprawzanie <trerść do sprawdzenia, konstruktor do walidacji?
    //spróbować na wzór tego z zadania 3, powinno zadziałać
    public static UserDataValidator zadanie() {
        return new UserDataValidator(new UsernameValidator(), new PasswordValidator());
    }

}
