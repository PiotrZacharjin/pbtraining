package lekcja1.zadania.zadanieCztery;

public class UserDataValidationResult {

    private boolean usernameValid;
    private boolean passwordValid;

    public UserDataValidationResult(boolean usernameValid, boolean passwordValid) {
        this.usernameValid = usernameValid;
        this.passwordValid = passwordValid;
    }

    public boolean isUsernameValid() {
        return usernameValid;
    }

    public boolean isPasswordValid() {
        return passwordValid;
    }
}
