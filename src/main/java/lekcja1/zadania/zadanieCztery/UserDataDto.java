package lekcja1.zadania.zadanieCztery;

public class UserDataDto {

    private final String username;
    private final String password;

    public UserDataDto(String username, String password){
        this.username = username;
        this.password = password;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
}
