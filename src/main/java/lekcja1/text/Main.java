package lekcja1.text;


import java.util.*;

public class Main {

    public static void main(String[] args) {
        //text jest reprezentowany przez klase String
        String text = "To jest text";
        String textTez = new String("to też");
        //na stringach można robić logiczne operacje takie jak
        text.contains("jest"); // zwraca true
        text.startsWith("To"); // zwraca true
        //na stringach można robić rozne operacje np pociecie na mniejsze stringi
        String[] elementy = "Piotrek,Tomek,Bartek".split(",");//zwraca tablice elementow Piotrek Tomek Bartek
        //Stringi można łaczycz z innymi zmiennymi
        String imie = "Piotrek";
        String el = imie + " zrobil "+ 8 + "pompek";// wyjdzie text Piotrek zrobil 8 pompek

        //Stringi dostarczaja rozna wbudowana funkcjonalnosc
        el.length(); // dlugosc stringa
        "Piotrek lubi prace w orange".replace("lubi","nienawidzi"); //podmieni lubi na nienawidzi

        System.out.println(zadanie("Piotr","Nowak", 30));
        System.out.println(zadanie("Adam","Psiuch", 23));

    }


    //ZADANIE
    // Pan Robot chciałby żebyś stworzył funkcje która przyjmuje imie ,nazwisko i wiek
    // podmienia imie Piotr na Anonim
    // zwraca {imie} {nazwisko} ma {wiek} lat. Imie ma długość {długość imienia}.
    public static String zadanie(String imie, String nazwisko, Integer wiek) {

        return null;
    }

}
