package lekcja1.switche;

import lekcja1.ify.Czlowiek;

public class Main {

    public static void main(String[] args) {
        String zmienna = "java";

        switch (zmienna){
            case "java":
                System.out.println("dobrze"); break;
            case "js":
                System.out.println("nopes"); break;
            default:
                System.out.println("domyslnie");
        }

        System.out.println(zadanie());
    }


    //ZADANIE
    // Robot chce zostać programista javy LUB pythona.
    // inaczej spierdoli
    // spraw by swith obslugiwal jave i pythona w zmiennej kariera
    // tylko ty mu na to pozwolisz

    //możesz też oszukać system i zrobić tak żeby kod przeszedł ;)
    public static Czlowiek zadanie() {
        Czlowiek robot = new Czlowiek();

        String kariera = "Pocztex";
        switch (kariera){
            default: robot.escapeFromEarth();
        }

        return robot;
    }


}
