package lekcja3.persistence.hibernate.crud_2.worker;

import lombok.Value;

@Value
public class WorkerDTO {

    private Long id;
    private String firstName;
    private String lastName;
    private Integer age;

    public static WorkerDTO empty() {
        return new WorkerDTO(-1L, "","", -1);
    }
}
