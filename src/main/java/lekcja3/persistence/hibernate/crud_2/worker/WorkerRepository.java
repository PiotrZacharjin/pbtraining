package lekcja3.persistence.hibernate.crud_2.worker;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface WorkerRepository extends CrudRepository<Worker,Long> {
}
