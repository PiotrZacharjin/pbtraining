package lekcja3.persistence.hibernate.crud_2.worker;

import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class WorkerReadService {

    private WorkerRepository repository;

    public List<WorkerDTO> getWorkers() {
        return StreamSupport
                .stream(repository.findAll().spliterator(), false)
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    public WorkerDTO getWorker(Long id) {
        return repository.findById(id).map(this::toDTO).orElse(WorkerDTO.empty());
    }


    @PreDestroy
    public void preDestroy(){
        System.out.println("worker read service self destruct");
    }

    @PostConstruct
    public void init(){
        System.out.println("worker read service post construct");
    }

    private WorkerDTO toDTO(Worker w){
        return new WorkerDTO(w.getId(), w.getFirstName(), w.getLastName(),w.getAge());
    }
}
