package lekcja3.persistence.hibernate.crud_2.worker;

import lekcja3.persistence.hibernate.crud_2.Client;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "worker")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Worker {


    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String firstName;
    private String lastName;
    private Integer age;

    @OneToMany(cascade = CascadeType.ALL,orphanRemoval = true)
    private List<Client> clients;
}
