package lekcja3.persistence.hibernate.crud_2.worker;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AddWorkerDTO {

    private String firstName;
    private String lastName;
    private Integer age;
}
