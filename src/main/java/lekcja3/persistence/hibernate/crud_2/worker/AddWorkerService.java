package lekcja3.persistence.hibernate.crud_2.worker;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@AllArgsConstructor
public class AddWorkerService {

    private WorkerRepository repository;
    private final ApplicationEventPublisher eventPublisher;
    public void add(AddWorkerDTO dto){
        Worker worker = new Worker(null,dto.getFirstName(), dto.getLastName(), dto.getAge(), new ArrayList<>());
        repository.save(worker);
        eventPublisher.publishEvent(new WorkerCreated(dto.getFirstName()+ " "+dto.getLastName()+ "created"));
    }
}
