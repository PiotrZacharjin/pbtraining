package lekcja3.persistence.hibernate.crud_2.worker;

import lombok.Value;

@Value
public class WorkerCreated {
   private String message;
}
