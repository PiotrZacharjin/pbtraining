package lekcja3.persistence.hibernate.crud_2.worker;

import com.sun.jndi.toolkit.url.Uri;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import java.net.MalformedURLException;
import java.net.URISyntaxException;

@ControllerAdvice
public class CreateExceptionHandler {

    @ResponseBody
    @ExceptionHandler(value = NotAuthenticated.class)
    public ResponseEntity<Uri> handle(NotAuthenticated exception) throws MalformedURLException, URISyntaxException {
        System.out.println("hello world");
        return ResponseEntity.ok().build();
    }

}
