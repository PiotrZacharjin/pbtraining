package lekcja3.persistence.hibernate.crud_2;

import lekcja3.persistence.hibernate.crud_2.worker.AddWorkerDTO;
import lekcja3.persistence.hibernate.crud_2.worker.WorkerCreated;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
public class MainApplication {


    public static void main(String... args){
        SpringApplication.run(MainApplication.class);
    }

    @Bean
    public CommandLineRunner create(){
        return (Args)->{
            ResponseEntity<String> stringResponseEntity = new RestTemplate().postForEntity("http://localhost:8080//worker", new AddWorkerDTO("Piotr", "Zacharjin", 20), String.class);
        };
    }

    @EventListener
    public void handleEvent(WorkerCreated event){
        System.out.println("hahahah to tak naprawde ja jestem Kira "+event.getMessage());
    }
}
