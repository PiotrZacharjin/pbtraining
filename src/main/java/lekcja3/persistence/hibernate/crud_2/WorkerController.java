package lekcja3.persistence.hibernate.crud_2;

import lekcja3.persistence.hibernate.crud_2.worker.AddWorkerDTO;
import lekcja3.persistence.hibernate.crud_2.worker.AddWorkerService;
import lekcja3.persistence.hibernate.crud_2.worker.WorkerDTO;
import lekcja3.persistence.hibernate.crud_2.worker.WorkerReadService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class WorkerController {

    private WorkerReadService workerService;
    private AddWorkerService addWorkerService;

    @GetMapping("/worker")
    public List<WorkerDTO> getWorkers(){
        return workerService.getWorkers();
    }

    @GetMapping("/worker/{id}")
    public ResponseEntity<WorkerDTO> getWorkers(@PathVariable Long id){
        WorkerDTO worker = workerService.getWorker(id);
        if(worker.equals(WorkerDTO.empty())){
            return ResponseEntity.ok(worker);
        }
        return ResponseEntity.notFound().build();
    }

    @PostMapping("/worker")
    @ExceptionHandler({RuntimeException.class})
    public ResponseEntity<HttpStatus> addWorker(@RequestBody AddWorkerDTO workerDTO){
        addWorkerService.add(workerDTO);
        return ResponseEntity.ok().build();
    }
}
