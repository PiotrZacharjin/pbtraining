package lekcja3.persistence.hibernate.entity_1;

import lekcja3.persistence.hibernate.entity_1.domain.NinjaEmployService;
import lekcja3.persistence.hibernate.entity_1.domain.NinjaRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class MainApplication {

    public static void main(String... args){
        SpringApplication.run(MainApplication.class);
    }

    @Bean
    public CommandLineRunner commandLineRunner(NinjaEmployService employService, NinjaRepository repository){
        return (args -> {
            employService.employ("Norauto", "kage-bushin", "rasengan");
            employService.employ("Saske", "chidori", "katon-bushin");
        });
    }
}
