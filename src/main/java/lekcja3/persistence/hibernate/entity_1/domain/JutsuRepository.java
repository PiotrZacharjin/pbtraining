package lekcja3.persistence.hibernate.entity_1.domain;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface JutsuRepository extends JpaRepository<Jutsu, JutsuId>, JpaSpecificationExecutor<Jutsu> {


    Optional<Jutsu> findByName(String name);
}
