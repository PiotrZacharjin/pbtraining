package lekcja3.persistence.hibernate.entity_1.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
@AllArgsConstructor
@Transactional
public class NinjaEmployService {
    private NinjaRepository repository;
    private JutsuService jutsuService;

    public void employ(String name, String... jutsu){
        List<Jutsu> jutsuList = Arrays.stream(jutsu).map(jutsuService::get).collect(Collectors.toList());
        Ninja ninja = new Ninja(null, name, jutsuList);
        repository.save(ninja);
        repository.findAll().forEach(System.out::println);
    }

}
