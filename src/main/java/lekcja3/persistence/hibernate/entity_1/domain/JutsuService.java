package lekcja3.persistence.hibernate.entity_1.domain;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;

@Service
@AllArgsConstructor
public class JutsuService {

    private JutsuRepository repository;
    private EntityManager en;

    public Jutsu get(String jutsu) {
        en.createQuery("SELECT e FROM Jutsu e WHERE name = :ramen").setParameter("ramen","chidori")
                .getResultStream().forEach(System.out::println);

        return repository.findByName(jutsu).orElse(
           new Jutsu(null, jutsu)
        );


    }


}
