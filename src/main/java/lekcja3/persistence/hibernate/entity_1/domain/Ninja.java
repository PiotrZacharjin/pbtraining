package lekcja3.persistence.hibernate.entity_1.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="ninja")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Ninja {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    private String name;

    @OneToMany(
            cascade = CascadeType.ALL, orphanRemoval = true
    )
    private List<Jutsu> jutsuList;
}
